<?php

use Faker\Generator as Faker;

$factory->define(App\SiteDetail::class, function (Faker $faker) {
    return [
        'site_name'=>$faker->domainName,
        'site_name_en'=>$faker->domainName,
        'address'=>$faker->address,
        'address_en'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'email'=>$faker->safeEmail,
    ];
});
