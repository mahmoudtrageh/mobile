<?php

use Faker\Generator as Faker;

$factory->define(App\Social::class, function (Faker $faker) {
    return [
        'facebook'=>$faker->text(90),
        'twitter'=>$faker->text(90),
        'instagram'=>$faker->text(90),
        'youtube'=>$faker->text(90),
        'snapchat'=>$faker->text(90),
        'whatsapp'=>$faker->text(90),
        'google_plus'=>$faker->text(90),
        'linked'=>$faker->text(90),
        'pinterest'=>$faker->text(90),
    ];
});
