<?php

use Faker\Generator as Faker;

$factory->define(App\SiteAbout::class, function (Faker $faker) {
    return [
        'img1'=>'123',
        'title'=>$faker->title,
        'title_en'=>$faker->title,
        'details'=>$faker->paragraph(3),
        'details_en'=>$faker->paragraph(3),
    ];
});
