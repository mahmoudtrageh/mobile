<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_id');
            $table->foreign('group_id')->references('id')->on('customer_groups')->onDelete('cascade');
            $table->string('name',100);
            $table->string('email',100);
            $table->string('phone',100);
            $table->string('password',100);
            $table->integer('status')->default(1);
            $table->integer('safe')->default(1);
            $table->integer('newsletter')->default(1);
            $table->string('gender',100)->nullable();
            $table->string('company')->nullable();
            $table->string('website')->nullable();
            $table->string('tracking_code')->nullable();
            $table->string('commission')->nullable();
            $table->string('tax_id')->nullable();
            $table->string('cn_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
