<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMobileConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mobile_conditions', function (Blueprint $table) {
            $table->renameColumn('cond1', 'cond1_ar');
            $table->renameColumn('cond2', 'cond1_en');
            $table->renameColumn('cond3', 'cond2_ar');
            $table->renameColumn('cond4', 'cond2_en');
            $table->renameColumn('cond5', 'cond3_ar');
            $table->renameColumn('cond6', 'cond3_en');
            $table->renameColumn('cond7', 'cond4_ar');
            $table->renameColumn('cond8', 'cond4_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
