<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_conditions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name_ar')->nullable();
            $table->string('name_en')->nullable();
            $table->string('cond1')->nullable();
            $table->string('cond2')->nullable();
            $table->string('cond3')->nullable();
            $table->string('cond4')->nullable();
            $table->string('cond5')->nullable();
            $table->string('cond6')->nullable();
            $table->string('cond7')->nullable();
            $table->string('cond8')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_conditions');
    }
}
