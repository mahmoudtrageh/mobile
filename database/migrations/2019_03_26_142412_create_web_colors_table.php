<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_colors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('first');
            $table->string('second');
            $table->string('third');
            $table->string('fourth');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web_colors');
    }
}
