<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('category_id')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->integer('manufacture_id')->unsigned();
            $table->integer('memory_id')->unsigned();
            $table->integer('network_id')->unsigned();
            $table->integer('condition_id')->unsigned();
            $table->string('img')->nullable();
            $table->string('value');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_products');
    }
}
