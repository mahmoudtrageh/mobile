<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('category_name')->nullable();
            $table->string('model_name')->nullable();
            $table->string('manufacture_name')->nullable();
            $table->string('memory')->nullable();
            $table->string('network')->nullable();
            $table->string('condition')->nullable();
            $table->string('store')->nullable();
            $table->string('img')->nullable();
            $table->string('value');
            $table->string('is_contacted')->default(0);
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->integer('phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_orders');
    }
}
