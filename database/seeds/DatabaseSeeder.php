<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name'=>'iki',
            'email'=>'ibrahem.kamal70@gmail.com',
            'password'=>bcrypt(123456),
            'isSuper'=>1,
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now(),
        ]);
        DB::table('admins')->insert([
            'name'=>'admin',
            'email'=>'admin@admin.com',
            'password'=>bcrypt(123456),
            'isSuper'=>1,
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now(),
        ]);
        factory(App\SiteDetail::class, 1)->create();
        factory(App\Social::class, 1)->create();
        factory(App\SiteAbout::class, 1)->create();
        // Working Time Table
        $days = [
            'Saturday',
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
        ];
        foreach ($days as $day) {
            DB::table('work_days')->insert([
                'day' => $day,
                'from' => '9',
                'to' => '5',
                'from_timing' => 'am',
                'to_timing' => 'pm',
            ]);
        }
        $roles = [
            [
                'ar' => 'المشرفين',
                'en' => 'Admins'
            ], [
                'ar' => 'المستخدمين',
                'en' => 'Users'
            ], [
                'ar' => 'الشرائح',
                'en' => 'Sliders'
            ], [
                'ar' => 'الإعدادات',
                'en' => 'Setting'
            ], [
                'ar' => 'الرسائل',
                'en' => 'Message'
            ], [
                'ar' => 'الدول',
                'en' => 'Country'
            ],
        ];
        foreach ($roles as $role) {
            DB::table('roles')->insert([
                'name_ar'=>$role['ar'],
                'name_en'=>$role['en'],
            ]);

        }

    }
}
