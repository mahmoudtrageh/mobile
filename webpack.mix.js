const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

// admin
mix.styles([
    'public/backEnd/assets/vendors/vendors.bundle.css',
    'public/backEnd/assets/app/app.bundle.css',
    'public/backEnd/assets/pages/datatables.css',
    'public/backEnd/css/my-style.css',
],'public/css/admin.css');



mix.scripts([
    'public/backEnd/assets/vendors/vendors.bundle.js',
    'public/backEnd/assets/app/app.bundle.js',
],'public/js/admin.js');

// site
mix.styles([
    'public/site/css/bootstrap.min.css',
    'public/site/css/font-awesome.min.css',
    'public/site/css/animate.css',
    'public/site/css/owl.carousel.css',
    'public/site/css/owl.theme.default.min.css',
    'public/site/css/magnific-popup.css',
    'public/site/css/templatemo-style.css',
],'public/css/site.css');



mix.scripts([
    'public/site/js/jquery.js',
    'public/site/js/bootstrap.min.js',
    'public/site/js/jquery.stellar.min.js',
    'public/site/js/wow.min.js',
    'public/site/js/owl.carousel.min.js',
    'public/site/js/jquery.magnific-popup.min.js',
    'public/site/js/smoothscroll.js',
    'public/site/js/custom.js',
],'public/js/site.js');
