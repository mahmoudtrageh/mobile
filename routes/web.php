<?php

/* Admin Routes */

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    /*
     *  admin login and logout routes
     */

    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {


        Route::get('/login', ['uses' => 'AuthController@index', 'as' => 'get.login']);

        Route::post('/login', ['uses' => 'AuthController@login', 'as' => 'post.login']);

        Route::get('/logout', ['uses' => 'AuthController@logout', 'as' => 'logout']);


    });


    /*Must Login Routes*/

    Route::group(['middleware' => 'admin'], function () {

        /*
         * Change Lang
         */
        Route::group(['as' => 'lang.'], function () {
            Route::get('/lang', ['uses' => 'IndexController@changeLang', 'as' => 'change']);
        });

        /*
        * Dashboard
        */
        Route::group(['as' => 'index.'], function () {
            Route::get('/', ['uses' => 'IndexController@index', 'as' => 'index']);
        });

        /*
                 * Profile
                 */
        Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
            Route::get('/', ['uses' => 'ProfileController@index', 'as' => 'index']);
            Route::post('/update', ['uses' => 'ProfileController@update', 'as' => 'update']);
            Route::post('/update-password', ['uses' => 'ProfileController@updatePassword', 'as' => 'update.password']);
        });


        /*
         * Admins
         */
        Route::group(['prefix' => 'admins', 'as' => 'admins.'], function () {

            Route::get('/', ['uses' => 'AdminsController@index', 'as' => 'index']);
            Route::post('/add-admin', ['uses' => 'AdminsController@add', 'as' => 'add']);
            Route::post('/edit-admin', ['uses' => 'AdminsController@edit', 'as' => 'edit']);
            Route::post('/delete-admin', ['uses' => 'AdminsController@delete', 'as' => 'delete']);

        });

        /*
         * Roles
         */
        Route::group(['prefix' => 'roles', 'as' => 'roles.'], function () {

            Route::get('/', ['uses' => 'RolesController@index', 'as' => 'index']);
            Route::post('/add-role', ['uses' => 'RolesController@add', 'as' => 'add']);
            Route::post('/edit-role', ['uses' => 'RolesController@edit', 'as' => 'edit']);
            Route::post('/delete-role', ['uses' => 'RolesController@delete', 'as' => 'delete']);

        });


        /*
        * Settings
        */
        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {

            Route::get('/', ['uses' => 'SettingsController@index', 'as' => 'index']);

            // Site details
            Route::post('/edit-details', ['uses' => 'SettingsController@editSiteDetails', 'as' => 'edit.details']);

            // Social
            Route::post('/edit-social', ['uses' => 'SettingsController@editSocial', 'as' => 'edit.social']);

            // About us
            Route::post('/edit-about', ['uses' => 'SettingsController@editAbout', 'as' => 'edit.about']);

            // Team
            Route::post('/add-team', ['uses' => 'SettingsController@addTeam', 'as' => 'add.team']);
            Route::post('/edit-team', ['uses' => 'SettingsController@editTeam', 'as' => 'edit.team']);
            Route::post('/delete-team', ['uses' => 'SettingsController@deleteTeam', 'as' => 'delete.team']);

            // gallery department
            Route::post('/add-gallery-department', ['uses' => 'SettingsController@addGalleryDepartment', 'as' => 'add.gallery.department']);
            Route::post('/edit-gallery-department', ['uses' => 'SettingsController@editGalleryDepartment', 'as' => 'edit.gallery.department']);
            Route::post('/delete-gallery-department', ['uses' => 'SettingsController@deleteGalleryDepartment', 'as' => 'delete.gallery.department']);


            // Gallery
            Route::post('/add-gallery', ['uses' => 'SettingsController@addGallrey', 'as' => 'add.gallery']);
            Route::post('/edit-gallery', ['uses' => 'SettingsController@editGallrey', 'as' => 'edit.gallery']);
            Route::post('/delete-gallery', ['uses' => 'SettingsController@deleteGallrey', 'as' => 'delete.gallery']);

            Route::post('/add-article', ['uses' => 'SettingsController@addArticle', 'as' => 'add.article']);
            Route::post('/edit-article', ['uses' => 'SettingsController@editArticle', 'as' => 'edit.article']);
            Route::post('/delete-article', ['uses' => 'SettingsController@deleteArticle', 'as' => 'delete.article']);

            // Services
            Route::post('/add-service', ['uses' => 'SettingsController@addService', 'as' => 'add.service']);
            Route::post('/edit-service', ['uses' => 'SettingsController@editService', 'as' => 'edit.service']);
            Route::post('/delete-service', ['uses' => 'SettingsController@deleteService', 'as' => 'delete.service']);

            // Web Colors
            Route::post('/edit-web-color', ['uses' => 'SettingsController@editWebColor', 'as' => 'edit.web.color']);

            // Mobile Colors
            Route::post('/edit-mobile-color', ['uses' => 'SettingsController@editMobileColor', 'as' => 'edit.mobile.color']);

            // Web Slider
            Route::post('/add-web-slider', ['uses' => 'SettingsController@addWebSlider', 'as' => 'add.web.slider']);
            Route::post('/edit-web-slider', ['uses' => 'SettingsController@editWebSlider', 'as' => 'edit.web.slider']);
            Route::post('/delete-web-slider', ['uses' => 'SettingsController@deleteWebSlider', 'as' => 'delete.web.slider']);
            Route::post('/web-slider-status', ['uses' => 'SettingsController@webSliderStatus', 'as' => 'web.slider.status']);

            // Mobile Slider
            Route::post('/add-mobile-slider', ['uses' => 'SettingsController@addMobileSlider', 'as' => 'add.mobile.slider']);
            Route::post('/edit-mobile-slider', ['uses' => 'SettingsController@editMobileSlider', 'as' => 'edit.mobile.slider']);
            Route::post('/delete-mobile-slider', ['uses' => 'SettingsController@deleteMobileSlider', 'as' => 'delete.mobile.slider']);
            Route::post('/mobile-slider-status', ['uses' => 'SettingsController@mobileSliderStatus', 'as' => 'mobile.slider.status']);

            Route::post('/add-testmonials', ['uses' => 'SettingsController@addTestmonials', 'as' => 'add.testmonials']);
            Route::post('/edit-testmonials', ['uses' => 'SettingsController@editTestmonials', 'as' => 'edit.testmonials']);
            Route::post('/delete-testmonials', ['uses' => 'SettingsController@deleteTestmonials', 'as' => 'delete.testmonials']);

            Route::post('/add-customer-logo', ['uses' => 'SettingsController@addCustomerLogo', 'as' => 'add.customer.logo']);
            Route::post('/edit-customer-logo', ['uses' => 'SettingsController@editCustomerLogo', 'as' => 'edit.customer.logo']);
            Route::post('/delete-customer-logo', ['uses' => 'SettingsController@deleteCustomerLogo', 'as' => 'delete.customer.logo']);

            Route::post('/add-condition', ['uses' => 'SettingsController@addCondition', 'as' => 'add.condition']);
            Route::post('/edit-condition', ['uses' => 'SettingsController@editCondition', 'as' => 'edit.condition']);
            Route::post('/delete-condition', ['uses' => 'SettingsController@deleteCondition', 'as' => 'delete.condition']);

            Route::post('/add-question', ['uses' => 'SettingsController@addQuestion', 'as' => 'add.question']);
            Route::post('/edit-question', ['uses' => 'SettingsController@editQuestion', 'as' => 'edit.question']);
            Route::post('/delete-question', ['uses' => 'SettingsController@deleteQuestion', 'as' => 'delete.question']);

            Route::post('/add-info', ['uses' => 'SettingsController@addInfo', 'as' => 'add.info']);
            Route::post('/edit-info', ['uses' => 'SettingsController@editInfo', 'as' => 'edit.info']);
            Route::post('/delete-info', ['uses' => 'SettingsController@deleteInfo', 'as' => 'delete.info']);

            Route::post('/add-shi-condition', ['uses' => 'SettingsController@addShCondition', 'as' => 'add.sh.condition']);
            Route::post('/edit-shi-condition', ['uses' => 'SettingsController@editShCondition', 'as' => 'edit.sh.condition']);
            Route::post('/delete-shi-condition', ['uses' => 'SettingsController@deleteShCondition', 'as' => 'delete.sh.condition']);

            // Time

            Route::post('/edit-time', ['uses' => 'SettingsController@editTime', 'as' => 'edit.time']);
            Route::post('/edit-status', ['uses' => 'SettingsController@editStatus', 'as' => 'edit.status']);

            Route::group(['prefix' => 'countries'], function () {

                Route::post('/add-country', ['uses' => 'SettingsController@AddCountry', 'as' => 'add.country']);
                Route::post('/edit-country', ['uses' => 'SettingsController@EditCountry', 'as' => 'edit.country']);
                Route::post('/delete-country', ['uses' => 'SettingsController@DeleteCountry', 'as' => 'delete.country']);

            });

            Route::group(['prefix' => 'provinces'], function () {

                Route::get('/{id}', ['uses' => 'SettingsController@Provinces', 'as' => 'provinces']);
                Route::post('/add-province', ['uses' => 'SettingsController@AddProvince', 'as' => 'add.province']);
                Route::post('/edit-province', ['uses' => 'SettingsController@EditProvince', 'as' => 'edit.province']);
                Route::post('/delete-province', ['uses' => 'SettingsController@DeleteProvince', 'as' => 'delete.province']);

            });

            Route::group(['prefix' => 'cities'], function () {

                Route::get('/{id}', ['uses' => 'SettingsController@Cities', 'as' => 'cities']);
                Route::post('/add-city', ['uses' => 'SettingsController@AddCity', 'as' => 'add.city']);
                Route::post('/edit-city', ['uses' => 'SettingsController@EditCity', 'as' => 'edit.city']);
                Route::post('/delete-city', ['uses' => 'SettingsController@DeleteCity', 'as' => 'delete.city']);

            });

            Route::group(['prefix' => 'areas'], function () {

                Route::get('/{id}', ['uses' => 'SettingsController@Areas', 'as' => 'areas']);
                Route::post('/add-area', ['uses' => 'SettingsController@AddArea', 'as' => 'add.area']);
                Route::post('/edit-area', ['uses' => 'SettingsController@EditArea', 'as' => 'edit.area']);
                Route::post('/delete-area', ['uses' => 'SettingsController@DeleteArea', 'as' => 'delete.area']);
            });
        });

        /*
       * Mobile
       */
        Route::group(['prefix' => 'mobile', 'as' => 'mobile.'], function () {

            Route::get('/', ['uses' => 'MobileController@index', 'as' => 'index']);

            // Category
            Route::post('/add-category', ['uses' => 'MobileController@addCategory', 'as' => 'add.category']);
            Route::post('/edit-category', ['uses' => 'MobileController@editCategory', 'as' => 'edit.category']);
            Route::post('/delete-category', ['uses' => 'MobileController@deleteCategory', 'as' => 'delete.category']);

            // Manufacture
            Route::post('/add-manufacture', ['uses' => 'MobileController@addManufacture', 'as' => 'add.manufacture']);
            Route::post('/edit-manufacture', ['uses' => 'MobileController@editManufacture', 'as' => 'edit.manufacture']);
            Route::post('/delete-manufacture', ['uses' => 'MobileController@deleteManufacture', 'as' => 'delete.manufacture']);

            // Module
            Route::post('/add-module', ['uses' => 'MobileController@addModule', 'as' => 'add.module']);
            Route::post('/edit-module', ['uses' => 'MobileController@editModule', 'as' => 'edit.module']);
            Route::post('/delete-module', ['uses' => 'MobileController@deleteModule', 'as' => 'delete.module']);

            // Memory
            Route::post('/add-memory', ['uses' => 'MobileController@addMemory', 'as' => 'add.memory']);
            Route::post('/edit-memory', ['uses' => 'MobileController@editMemory', 'as' => 'edit.memory']);
            Route::post('/delete-memory', ['uses' => 'MobileController@deleteMemory', 'as' => 'delete.memory']);

            // Memory
            Route::post('/add-network', ['uses' => 'MobileController@addNetwork', 'as' => 'add.network']);
            Route::post('/edit-network', ['uses' => 'MobileController@editNetwork', 'as' => 'edit.network']);
            Route::post('/delete-network', ['uses' => 'MobileController@deleteNetwork', 'as' => 'delete.network']);

            // Memory
            Route::post('/add-mobile-condition', ['uses' => 'MobileController@addMobileCondition', 'as' => 'add.mobile.condition']);
            Route::post('/edit-mobile-condition', ['uses' => 'MobileController@editMobileCondition', 'as' => 'edit.mobile.condition']);
            Route::post('/delete-mobile-condition', ['uses' => 'MobileController@deleteMobileCondition', 'as' => 'delete.mobile.condition']);

            // Memory
            Route::post('/add-store', ['uses' => 'MobileController@addStore', 'as' => 'add.store']);
            Route::post('/edit-store', ['uses' => 'MobileController@editStore', 'as' => 'edit.store']);
            Route::post('/delete-store', ['uses' => 'MobileController@deleteStore', 'as' => 'delete.store']);

            // Memory
            Route::post('/add-sell-product', ['uses' => 'MobileController@addSellProduct', 'as' => 'add.sell.product']);
            Route::post('/edit-sell-product', ['uses' => 'MobileController@editSellProduct', 'as' => 'edit.sell.product']);
            Route::post('/delete-sell-product', ['uses' => 'MobileController@deleteSellProduct', 'as' => 'delete.sell.product']);

            // Memory
            Route::post('/add-buy-product', ['uses' => 'MobileController@addBuyProduct', 'as' => 'add.buy.product']);
            Route::post('/edit-buy-product', ['uses' => 'MobileController@editBuyProduct', 'as' => 'edit.buy.product']);
            Route::post('/delete-buy-product', ['uses' => 'MobileController@deleteBuyProduct', 'as' => 'delete.buy.product']);

            Route::post('delete-order-sell', 'MobileController@DeleteSell')->name('delete.sell');
            Route::post('/edit-sell-status', ['uses' => 'MobileController@editSell', 'as' => 'edit.sell']);

            Route::post('delete-order-buy', 'MobileController@deleteBuy')->name('delete.buy');
            Route::post('/edit-buy-status', ['uses' => 'MobileController@editBuy', 'as' => 'edit.buy']);

        });
        /*
         * Messages
         */
        Route::group(['prefix' => 'messages', 'as' => 'messages.'], function () {
            Route::get('/', ['uses' => 'MessagesController@index', 'as' => 'index']);
            Route::post('/delete-message', ['uses' => 'MessagesController@delete', 'as' => 'delete']);
            Route::post('/delete-all', ['uses' => 'MessagesController@deleteAll', 'as' => 'delete.all']);

        });




    });
});

Route::group(['namespace' => 'Site'], function () {

    /*
     * Index
     */

    Route::post('send-message','IndexController@sendMessage')->name('send.message');


    Route::get('', 'IndexController@index')->name('site-index');
    Route::get('buy', 'IndexController@buy')->name('site.buy');
    Route::get('department/{id}', 'IndexController@department')->name('site.department');
    Route::get('model/{id}', 'IndexController@model')->name('site.model');
    Route::get('capacity', 'IndexController@capacity')->name('site.capacity');
    Route::get('signal', 'IndexController@signal')->name('site.signal');
    Route::get('status', 'IndexController@status')->name('site.status');
    Route::get('details', 'IndexController@details')->name('site.details');
    Route::get('branch', 'IndexController@branch')->name('site.branch');
    Route::post('send-order', 'IndexController@sendOrder')->name('site.send.order');


    // sell

    Route::get('sell', 'IndexController@sell')->name('site.sell');
    Route::get('department-buy/{id}', 'IndexController@departmentBuy')->name('site.department.buy');
    Route::get('model-buy/{id}', 'IndexController@modelBuy')->name('site.model.buy');
    Route::get('capacity-buy', 'IndexController@capacityBuy')->name('site.capacity.buy');
    Route::get('signal-buy', 'IndexController@signalBuy')->name('site.signal.buy');
    Route::get('status-buy', 'IndexController@statusBuy')->name('site.status.buy');
    Route::get('details-buy', 'IndexController@detailsBuy')->name('site.details.buy');
    Route::get('branch-buy', 'IndexController@branchBuy')->name('site.branch.buy');
    Route::post('sell-mobile', 'IndexController@sellMobile')->name('site.sell.mobile');


    Route::post('get-condition', 'IndexController@getCondition')->name('site.get.condition');

});