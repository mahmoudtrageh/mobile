<!DOCTYPE html>

<html lang="en" class="smart-style-1">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,500,700">
    <link rel="shortcut icon" href="{{site_details()->icon ? site_details()->icon : asset('default-logo.jpg')}}  "
          type="image/x-icon">
    <link rel="icon" href="{{site_details()->icon ? site_details()->icon : asset('default-logo.jpg')}}  "
          type="image/x-icon">

    <link href="{{ asset('css/admin.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('site/css/font-awesome.min.css')}}" media="all" rel="stylesheet" type="text/css"/>

    @if (app()->getLocale() == 'ar')
        <link href="{{ asset('backEnd/css/smartadmin-rtl.min.css')}}" media="all" rel="stylesheet" type="text/css"/>
    @endif
</head>

<body class="smart-style-1 " @if (app()->getLocale() == 'ar') dir="rtl" @endif>

<!-- BEGIN .sa-wrapper -->
<div class="sa-wrapper">
    <!-- BEGIN .sa-shortcuts -->

    <div class="sa-shortcuts-section">
        <ul>
            <li><a class="bg-pink-dark selected" href="{{route('admin.profile.index')}}"><span
                            class="fa fa-user fa-4x"></span><span
                            class="box-caption">{{trans('admin.profile')}}</span><em class="counter"></em></a></li>
        </ul>
    </div>
    <!-- END .sa-shortcuts -->

    <header class="sa-page-header">
        <div class="sa-header-container h-100">
            <div class="d-table d-table-fixed h-100 w-100">
                <div class="sa-logo-space d-table-cell h-100">
                    <div class="flex-row d-flex align-items-center h-100">
                        <a class="sa-logo-link" href="" title="Smart Admin 2.0"><img alt="Smart Admin 2.0"
                                                                                     src="{{site_details()->logo ? asset('uploads/site_details/' . site_details()->logo) :  asset('default-logo.jpg')}}"
                                                                                     class="sa-logo"></a>
                        <div class="dropdown ml-auto">
                            <button class="btn btn-default sa-btn-icon sa-activity-dropdown-toggle" type="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                        class="fa fa-user"></span><span class="badge bg-red">0</span></button>
                            <div class="dropdown-menu ml-auto ajax-dropdown" aria-labelledby="dropdownMenuButton">
                                <form class="btn-group btn-group-justified" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-default"> (0)
                                    </button>
                                </form>
                                <ul>
                                    <li>message 1</li>
                                    <li>message 2</li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-table-cell h-100 w-100 align-middle">
                    <div class="sa-header-menu">
                        <div class="d-flex align-items-center w-100">
                            <div class="ml-auto sa-header-right-area">
                                <div class="form-inline">
										<span class="dropdown sa-country-dropdown">
											<a href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true"
                                               aria-expanded="false"><em class="flag flag-us"></em> <span> @if (app()->getLocale() == 'ar')
                                                        {{trans('admin.lang.ar')}}
                                                    @else
                                                        {{trans('admin.lang.en')}}
                                                    @endif
                                                     <span class="fa fa-angle-down"></span></span></a>
                                             <span class="dropdown-menu dropdown-menu-right ml-auto"
                                                   aria-labelledby="dropdownMenuButton">
                                                 <a class="dropdown-item @if (app()->getLocale() == 'en') active @endif "
                                                    href="{{route('admin.lang.change',['lang'=>'en'])}}"><span
                                                             class="flag flag-us"></span>
                                                     English</a>
												<a class="dropdown-item @if (app()->getLocale() == 'ar') active @endif "
                                                   href="{{route('admin.lang.change',['lang'=>'ar'])}}"><span
                                                            class="flag flag-fr"></span>
													العربيه</a>

											</span>
										</span>
                                    {{--                                    {{dd(app()->getLocale())}}--}}
                                    <button
                                            class="btn btn-light sa-btn-icon sa-toggle-full-screen d-none d-lg-block"
                                            type="button" onclick="toggleFullScreen()"><span
                                                class="fa fa-arrows-alt"></span></button>

                                    <a class="btn btn-default  sa-btn-icon"
                                       type="button" href="{{route('admin.auth.logout')}}"><span
                                                class="fa fa-sign-out"></span></a>
                                    <button class="btn btn-default sa-btn-icon sa-sidebar-hidden-toggle"
                                            onclick="SAtoggleClass(this, 'body', 'sa-hidden-menu')" type="button"><span
                                                class="fa fa-reorder"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- END .sa-page-header -->
    <div class="sa-page-body">
        <!-- BEGIN .sa-aside-left -->
        <div class="sa-aside-left">
            <a href="javascript:void(0)" onclick="SAtoggleClass(this, 'body', 'sa-shortcuts-expanded')"
               class="sa-sidebar-shortcut-toggle">
                <img src="{{auth()->guard('admin')->user()->img ? asset('uploads/admin/avatar/'.auth()->guard('admin')->user()->img) : asset('default.png')}}  "
                     alt="" class="online">
                <span>{{auth()->guard('admin')->user()->name}} <span class="fa fa-angle-down"></span></span>
            </a>
            <div class="sa-left-menu-outer">
                <ul class="metismenu sa-left-menu" id="menu1">

                    @if (adminPermissions(1))
                        <li class=" @if (sidebarActive('admin.admins.index')) active @endif ">
                            <!-- first-level -->
                            <a href="{{route('admin.admins.index')}}" title="Outlook"><span
                                        class="fa fa-lg fa-fw fa-inbox"></span>
                                <span class="menu-item-parent">{{trans('admin.admins')}}</span></a>
                        </li>
                    @endif

                        <li class=" @if (sidebarActive('admin.mobile.index')) active @endif ">
                            <!-- first-level -->
                            <a href="{{route('admin.mobile.index')}}" title="Outlook"><span
                                        class="fa fa-lg fa-fw fa-inbox"></span>
                                <span class="menu-item-parent">{{trans('admin.mobile')}}</span></a>
                        </li>

                    {{-- <li class=" ">
                         <!-- first-level -->
                         <a href="{{route('admin.roles.index')}}" title="Outlook"><span
                                     class="fa fa-lg fa-fw fa-inbox"></span>
                             <span class="menu-item-parent">roles</span></a>
                     </li>--}}

                    @if (adminPermissions(4))
                        <li class=" @if (sidebarActive('admin.settings.index'))active @endif ">
                            <!-- first-level -->
                            <a href="{{route('admin.settings.index')}}" title="Outlook"><span
                                        class="fa fa-lg fa-fw fa-gear"></span> <span
                                        class="menu-item-parent">{{trans('admin.settings')}}</span></a>
                        </li>
                    @endif
                    @if (adminPermissions(5))
                        <li class=" @if (sidebarActive('admin.messages.index'))active @endif ">
                            <!-- first-level -->
                            <a href="{{route('admin.messages.index')}}" title="Outlook"><span
                                        class="fa fa-lg fa-fw fa-envelope-square"></span> <span
                                        class="menu-item-parent">{{trans('admin.messages')}}</span></a>
                        </li>
                    @endif

                </ul>
            </div>
            <a href="javascript:void(0)" class="minifyme" onclick="SAtoggleClass(this, 'body', 'minified')">
                <i class="fa fa-arrow-circle-left hit"></i>
            </a>
        </div>
        <!-- BEGIN .sa-content-wrapper -->
    @yield('content')
    <!-- END .sa-content-wrapper -->


    </div>
    <!-- BEGIN .sa-page-footer -->
    <footer class="sa-page-footer" style="position: fixed;bottom: 0;z-index: 8;">
        <div class="d-flex align-items-center w-100 h-100" style="text-align: center">
            <div class="footer-left w-100">
                SpectraApp &copy;
                2019
            </div>

        </div>
    </footer>
    <!-- END .sa-page-footer -->
</div>

<!-- END .sa-wrapper -->
<script type="text/javascript" src="{{asset('backEnd/sweetalert2.all.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/admin.js')}}"></script>


<script>
    $(function () {
        $('#menu1').metisMenu();
    });
</script>

<script type="text/javascript">

    /* BASIC ;*/
    var responsiveHelper_dt_basic = responsiveHelper_dt_basic || undefined;
    var responsiveHelper_datatable_fixed_column = responsiveHelper_datatable_fixed_column || undefined;
    var responsiveHelper_datatable_col_reorder = responsiveHelper_datatable_col_reorder || undefined;
    var responsiveHelper_datatable_tabletools = responsiveHelper_datatable_tabletools || undefined;

    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };


    /* END COLUMN SHOW - HIDE */

    /* TABLETOOLS */
    $('#datatable_tabletools').dataTable({

        // Tabletools options:
        //   https://datatables.net/extensions/tabletools/button_options
        "sDom": "<'dt-toolbar d-flex'<f><'hidden-xs ml-auto'B>r>" +
            "t" +
            "<'dt-toolbar-footer d-flex'<'hidden-xs'i><'ml-auto'p>>",
        "oLanguage": {
            "sSearch": '<span class="input-group-addon"><i class="fa fa-search"></i></span>'
        },
        "classes": {
            "sW`rapper": "dataTables_wrapper dt-bootstrap4"
        },
        buttons: [{
            extend: 'print',
            className: 'btn btn-default'
        }],
        "autoWidth": true

    });

    /* END TABLETOOLS */


</script>
<script>
    @if(session()->has('success'))
    Swal.fire({
        position: 'center',
        type: 'success',
        title: '{{session()->get('success')}}',
        showConfirmButton: false,
        width: '40rem',
        customClass: "right-check",
        timer: 1500
    });
    @endif
    @if(session()->has('error'))
    Swal.fire({
        type: 'error',
        title: 'حدث خطأ',
        text: '{{session()->get('error')}}',
    });
    @endif
    {{--@if ($errors->any())
    Swal.fire({
        title: '<strong style="font-size: 25px; font-weight: lighter;"> برجاء إصلاح الأخطاء التاليه</strong>',
        type: 'info',
        html: '<p style="font-weight: bold;font-size: 15px;color: red;">' + '{{$errors->first()}}' + '</p>',
        width: '40rem',
    });
    @endif--}}
</script>

<script>
    $(document).ready(function () {
        $('#dt-basic').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
    });
</script>
<script>
    $('input[type="file"]').each(function () {

        var $file = $(this),
            $label = $file.next('label'),
            $labelText = $label.find('span'),
            labelDefault = $labelText.text();

        $file.on('change', function (event) {
            var fileName = $file.val().split('\\').pop(),
                tmppath = URL.createObjectURL(event.target.files[0]);
            if (fileName) {
                $label
                    .addClass('file-ok')
                    .css('background-image', 'url(' + tmppath + ')');
                $labelText.text(fileName);
            } else {
                $label.removeClass('file-ok');
                $labelText.text(labelDefault);
            }
        });

    });
</script>
@yield('scripts')
</body>

</html>