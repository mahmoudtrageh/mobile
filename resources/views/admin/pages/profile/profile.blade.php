@extends('admin.layouts.Master-Layout')

@section('title')

    {{trans('admin.profile')}}

@stop

@section('content')
    <div class="sa-content-wrapper">

        <div class="sa-content">
            <div class="d-flex w-100 home-header">
                <div>
                    <h1 class="page-header"><i class="fa fa-table fa-fw "></i> {{trans('admin.dashboard')}} <span>>  {{trans('admin.profile')}}
								</span></h1>
                </div>
            </div>
            <div>
                <div>
                    <!-- widget grid -->
                    <section id="widget-grid" class="">
                        <!-- row -->
                        <div class="row">
                            <!-- NEW WIDGET START -->
                            <article class="col-12">
                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-3"
                                     data-widget-editbutton="false">
                                    <header>
                                        <div class="widget-header">
													<span class="widget-icon"> <i
                                                                class="fa fa-lg fa-fw fa-user"></i> </span>
                                        </div>
                                    </header>
                                    <!-- END RIBBON -->
                                    <div id="content" style="margin-top:2rem;">

                                        <!-- row -->
                                        <div class="row">

                                            <div class="col-sm-12">


                                                <div class="well well-sm">

                                                    <div class="row">

                                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                                            <div
                                                                    class="well well-light well-sm no-margin no-padding">

                                                                <div class="row">
                                                                    <div class="col-sm-12">

                                                                        <div class="row">


                                                                            <div class="col-sm-6">
                                                                                <form class="form-horizontal"
                                                                                      action="{{route('admin.profile.update')}}"
                                                                                      method="post"
                                                                                      enctype="multipart/form-data"
                                                                                      style="margin-top: 5rem;padding-right: 2rem;">
                                                                                    @csrf
                                                                                    <div class="form-group">
                                                                                        <div
                                                                                                class="col-sm-6 profile-pic">
                                                                                            <div class="wrap-custom-file ">
                                                                                                <input type="file"
                                                                                                       name="img"
                                                                                                       id="eidtimg"
                                                                                                       accept=".gif, .jpg, .png"/>
                                                                                                <label for="eidtimg"
                                                                                                       class="file-ok"
                                                                                                       style="background-image: url({{$user->img ? asset('uploads/admin/avatar/'.$user->img) : asset('default.png')}});">
							                                                                    <span><i class="fa fa-file-image-o "
                                                                                                         style="font-size:5rem;"></i></span>
                                                                                                </label>
                                                                                            </div>
                                                                                            <h4 style="font-weight: bold;margin-top:1rem;"> {{$user->name}} </h4>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text"
                                                                                                   class="form-control"
                                                                                                   id="inputEmail33"
                                                                                                   placeholder=" {{trans('admin.name')}} "
                                                                                                   name="name"
                                                                                                   value="{{$user->name}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">

                                                                                        <div class="col-sm-10">
                                                                                            <input type="email"
                                                                                                   class="form-control"
                                                                                                   id="inputEmail32"
                                                                                                   placeholder="{{trans('admin.email')}}"
                                                                                                   name="email"
                                                                                                   value="{{$user->email}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class=" col-sm-12"
                                                                                         style="margin-bottom:2rem;">
                                                                                        <button
                                                                                                class="btn btn-info reset"
                                                                                                data-toggle="modal"
                                                                                                data-target="#myModal">
                                                                                            {{trans('admin.change.password')}}
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <div class=" col-sm-12"
                                                                                             style="text-align:right;margin-bottom: 1rem;">

                                                                                            <button type="submit"
                                                                                                    class="btn btn-primary">
                                                                                                {{trans('admin.confirm')}}
                                                                                            </button>
                                                                                        </div>

                                                                                    </div>
                                                                                </form>


                                                                                <!-- Modal -->
                                                                                <div class="modal fade"
                                                                                     id="myModal"
                                                                                     tabindex="-1"
                                                                                     role="dialog"
                                                                                     aria-labelledby="myModalLabel">
                                                                                    <div class="modal-dialog"
                                                                                         role="document">
                                                                                        <div
                                                                                                class="modal-content">
                                                                                            <div
                                                                                                    class="modal-header">
                                                                                                <h4 class="modal-title"
                                                                                                    id="myModalLabel"
                                                                                                    style="font-weight:bold;">
                                                                                                    {{trans('admin.change.password')}}
                                                                                                </h4>
                                                                                                <button
                                                                                                        type="button"
                                                                                                        class="close"
                                                                                                        data-dismiss="modal"
                                                                                                        aria-label="Close"><span
                                                                                                            aria-hidden="true">&times;</span>
                                                                                                </button>

                                                                                            </div>
                                                                                            <form action="{{route('admin.profile.update.password')}}"
                                                                                                  method="post">
                                                                                                @csrf

                                                                                                <div
                                                                                                        class="modal-body">

                                                                                                    <div
                                                                                                            class="form-group">
                                                                                                        <input
                                                                                                                type="password" autocomplete="off"
                                                                                                                class="form-control"
                                                                                                                id="inputEmail311"
                                                                                                                placeholder="{{trans('admin.old.password')}} "
                                                                                                                name="old_password">
                                                                                                    </div>
                                                                                                    <div
                                                                                                            class="form-group">
                                                                                                        <input
                                                                                                                type="password" autocomplete="off"
                                                                                                                class="form-control"
                                                                                                                id="inputEmail378"
                                                                                                                placeholder=" {{trans('admin.new.password')}} "
                                                                                                                name="new_password">
                                                                                                    </div>
                                                                                                    <div
                                                                                                            class="form-group">
                                                                                                        <input
                                                                                                                type="password" autocomplete="off"
                                                                                                                class="form-control"
                                                                                                                id="inputEmail3364"
                                                                                                                placeholder=" {{trans('admin.confirm.password')}} "
                                                                                                                name="new_password_confirmation">
                                                                                                    </div>


                                                                                                </div>
                                                                                                <div
                                                                                                        class="modal-footer">
                                                                                                    <button
                                                                                                            type="button"
                                                                                                            class="btn btn-default"
                                                                                                            data-dismiss="modal">
                                                                                                        {{trans('admin.cancel')}}
                                                                                                    </button>
                                                                                                    <button
                                                                                                            type="submit"
                                                                                                            class="btn btn-primary">
                                                                                                        {{trans('admin.confirm')}}
                                                                                                    </button>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>


                                            </div>

                                        </div>

                                        <!-- end row -->

                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget -->

                            </article>
                            <!-- WIDGET END -->

                        </div>

                        <!-- end row -->

                        <!-- end row -->

                    </section>
                    <!-- end widget grid -->

                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        $(document).on('click', '.reset', function (e) {
            e.preventDefault()
        })
    </script>
@stop
