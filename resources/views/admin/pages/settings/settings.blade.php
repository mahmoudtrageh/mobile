@extends('admin.layouts.Master-Layout')

@section('title')

    {{trans('admin.settings')}}

@stop

@section('content')
    <div class="sa-content-wrapper">

        <div class="sa-content">
            <div class="d-flex w-100 home-header">
                <div>
                    <h1 class="page-header"><i class="fa fa-table fa-fw "></i> {{trans('admin.dashboard')}} <span>> {{trans('admin.settings')}}
								</span></h1>
                </div>
            </div>
            <div>
                <div>

                    <!-- widget grid -->
                    <section id="widget-grid" class="">
                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="well well-sm well-light">

                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#Details" role="tab">{{trans('admin.details')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#media" role="tab">{{trans('admin.social')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#About" role="tab">{{trans('admin.about')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Team" role="tab">{{trans('admin.team')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Department" role="tab">{{trans('admin.department')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Gallary" role="tab">{{trans('admin.gallery')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Articles" role="tab">{{trans('admin.article')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Services" role="tab">{{trans('admin.services')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Time" role="tab">{{trans('admin.time')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Country" role="tab">{{trans('admin.country')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Color" role="tab">{{trans('admin.color')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Web" role="tab">{{trans('admin.web-slider')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Mobile" role="tab">{{trans('admin.mobile-slider')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Testimonial" role="tab">{{trans('admin.testmonials')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#clogo" role="tab">{{trans('admin.clogo')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#conditions" role="tab">{{trans('admin.condition')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#sh-conditions" role="tab">{{trans('admin.sh conditions')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#questions" role="tab">{{trans('admin.questions')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#info" role="tab">{{trans('admin.info')}}</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="Details" role="tabpanel">
                                            <form action="{{route('admin.settings.edit.details')}}" method="post" style="margin-top:1rem" enctype="multipart/form-data">

                                                @csrf
                                                <div class="row">

                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                        <input type="text" name="site_name" class="form-control" id="exampleInputText1" placeholder="Websit Arabic Name" value="{{$site_details->site_name}}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                        <input name="site_name_en" type="text" class="form-control" id="exampleInputText1" placeholder="Websit Englis Name" value="{{$site_details->site_name_en}}">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">{{trans('admin.arabic address')}}</label>
                                                        <input name="address" type="text" class="form-control" id="exampleInputText1" placeholder="Arabic Address" value="{{$site_details->address}}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">{{trans('admin.english address')}}</label>
                                                        <input  name="address_en" type="text" class="form-control" id="exampleInputText1" placeholder="English Address" value="{{$site_details->address_en}}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">{{trans('admin.phone')}}</label>
                                                        <input name="phone" type="number" class="form-control" id="exampleInputText1" placeholder="Phone" value="{{$site_details->phone}}">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">{{trans('admin.email')}}</label>
                                                        <input name="email" type="email" class="form-control" id="exampleInputText1" placeholder="Email" value="{{$site_details->email}}">
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">{{trans('admin.map')}}</label>
                                                        <input name="map" type="text" class="form-control" id="exampleInputText1" placeholder="https://www.google.com/maps" value="{{$site_details->map}}" >
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>{{trans('admin.logo')}}</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="logo" id="logo"
                                                                   accept=".gif, .jpg, .png"/>
                                                            <label for="logo" class="file-ok"
                                                                   style="background-image: url({{$site_details->logo ?  url('uploads/site_details/'.$site_details->logo) : asset('default-logo.jpg')}});">
                                                            <span><i class="fa fa-file-image-o "
                                                                     style="font-size:5rem;"></i></span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>{{trans('admin.icon')}}</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="icon" id="icon"
                                                                   accept=".gif, .jpg, .png"/>
                                                            <label for="icon" class="file-ok"
                                                                   style="background-image: url({{$site_details->icon ?  url('uploads/site_details/'.$site_details->icon) : asset('default-logo.jpg')}});">
                                                            <span><i class="fa fa-file-image-o "
                                                                     style="font-size:5rem;"></i></span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer" style="margin-top:1rem;">
                                                    <button type="button" class="btn btn-default">{{trans('admin.close')}}</button>
                                                    <button type="submit" class="btn btn-primary">{{trans('admin.confirm')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="media" role="tabpanel">
                                            <form action="{{route('admin.settings.edit.social')}}" method="post">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.facebook')}}<i
                                                                class="fa fa-facebook-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.facebook')}}" name="facebook"
                                                           value="{{$social->facebook}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.twitter')}} <i
                                                                class="fa fa-twitter-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.twitter')}}" name="twitter"
                                                           value="{{$social->twitter}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.instagram')}} <i
                                                                class="fa fa-instagram"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.instagram')}}" name="instagram"
                                                           value="{{$social->instagram}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.youtube')}} <i
                                                                class="fa fa-youtube-play"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.youtube')}}" name="youtube"
                                                           value="{{$social->youtube}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.snapchat')}} <i
                                                                class="fa fa-snapchat-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.snapchat')}}" name="snapchat"
                                                           value="{{$social->snapchat}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.whatsapp')}} <i
                                                                class="fa fa-whatsapp"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.whatsapp')}} " name="whatsapp"
                                                           value="{{$social->whatsapp}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.google.plus')}} <i
                                                                class="fa fa-google-plus-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.google.plus')}}" name="google_plus"
                                                           value="{{$social->google_plus}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.linkedin')}} <i
                                                                class="fa fa-linkedin-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.linkedin')}}" name="linked"
                                                           value="{{$social->linked}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.pinterest')}} <i
                                                                class="fa fa-pinterest-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.pinterest')}}" name="pinterest"
                                                           value="{{$social->pinterest}}">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">{{trans('admin.confirm')}}
                                                    </button>
                                                    <button type="button" class="btn btn-default">{{trans('admin.close')}}</button>

                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="About" role="tabpanel">
                                            <form style="margin-top:1rem" action="{{route('admin.settings.edit.about')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>{{trans('admin.about us img 1')}}</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="img1" id="img1" accept=".gif, .jpg, .png" />
                                                            <label for="img1" class="file-ok" style="background-image: url({{$about->img1 ?  url('uploads/site_about/'. $about->img1) : asset('default-logo.jpg')}});">
                                                                <span>{{trans('admin.upload img')}}</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>{{trans('admin.about us img 2')}}</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="img2" id="img2" accept=".gif, .jpg, .png" />
                                                            <label for="img2" class="file-ok" style="background-image: url({{$about->img2 ?  url('uploads/site_about/'. $about->img2) : asset('default-logo.jpg')}});">
                                                                <span>{{trans('admin.upload img')}}</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>{{trans('admin.about us img 3')}}</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="img3" id="img3" accept=".gif, .jpg, .png" />
                                                            <label for="img3" class="file-ok" style="background-image: url({{$about->img3 ?  url('uploads/site_about/'. $about->img3) : asset('default-logo.jpg')}});">
                                                                <span>{{trans('admin.upload img')}}</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                            <label>{{trans('admin.about us img 4')}}</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="img4" id="img4" accept=".gif, .jpg, .png" />
                                                            <label for="img4" class="file-ok" style="background-image: url({{$about->img4 ?  url('uploads/site_about/'. $about->img4) : asset('default-logo.jpg')}});">
                                                                <span>{{trans('admin.upload img')}}</span>
                                                            </label>
                                                        </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                            <label>{{trans('admin.about us img 5')}}</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="img5" id="img5" accept=".gif, .jpg, .png" />
                                                            <label for="img5" class="file-ok" style="background-image: url({{$about->img5 ?  url('uploads/site_about/'. $about->img5) : asset('default-logo.jpg')}});">
                                                                <span>{{trans('admin.upload img')}}</span>
                                                            </label>
                                                        </div>
                                                        </div>
                                                            </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputEmail1">{{trans('admin.arabic title')}}</label>
                                                            <input type="text" name="title" value="{{$about->title}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic title')}}">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputEmail1">{{trans('admin.english title')}}</label>
                                                            <input type="text" name="title_en" class="form-control" value="{{$about->title_en}}" id="exampleInputText1" placeholder="{{trans('admin.english title')}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleFormControlTextarea1">{{trans('admin.about us in arabic')}}</label>
                                                        <textarea class="form-control" name="details" id="exampleFormControlTextarea1" rows="3" placeholder="{{trans('admin.about us in arabic')}}">{{$about->details}}</textarea>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleFormControlTextarea1">{{trans('admin.about us in english')}}</label>
                                                        <textarea class="form-control" name="details_en" id="exampleFormControlTextarea1" rows="3" placeholder="{{trans('admin.about us in english')}}">{{$about->details_en}}</textarea>
                                                    </div>
                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                        <button type="button" class="btn btn-default">{{trans('admin.close')}}</button>
                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                    </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="Team" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-1">{{trans('admin.add member')}}</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add member')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.team')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">

                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                <input type="text" name="name" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                            </div>

                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">{{trans('admin.job title in english')}}</label>
                                                                                <input type="text" name="title_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.job title in english')}}">
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">{{trans('admin.job title in arabic')}}</label>
                                                                                <input type="text"  name="title" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.job title in arabic')}}">
                                                                            </div>

                                                                            <div class="wrap-custom-file ">
                                                                                <input type="file" name="img" id="image9" accept=".gif, .jpg, .png" />
                                                                                <label for="image9" class="file-ok">
                                                                                    <span>{{trans('admin.image')}}</span>
                                                                                </label>
                                                                            </div>
                                                                    </div>

                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        @foreach($teams as $team)

                                                            <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>

                                                            <th data-hide="phone">{{trans('admin.job title in arabic')}}</th>
                                                            <th data-hide="phone">{{trans('admin.job title in english')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>


                                                        <tr>

                                                            <td>{{$team->id}}</td>
                                                            <td>{{$team->name}}</td>
                                                            <td>{{$team->name_en}}</td>
                                                            <td>{{$team->title}}</td>
                                                            <td>{{$team->title_en}}</td>

                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/team/'. $team->img)}}" data-img="{{asset('uploads/team/'. $team->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-job{{$team->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-deljob{{$team->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                        @endforeach


                                                        @foreach($teams as $team)
                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-job{{$team->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.edit')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.team',['id'=>$team->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name" value="{{$team->name}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$team->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.job title in english')}}</label>
                                                                                    <input type="text" name="title_en" value="{{$team->title_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.job title in english')}}">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.job title in arabic')}}</label>
                                                                                    <input type="text" name="title" value="{{$team->title}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.job title in arabic')}}">
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image9{{$team->id}}" accept=".gif, .jpg, .png" />
                                                                                    <label for="image9{{$team->id}}"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/team/'.$team->img)}});">
                                                                                        <span>{{trans('admin.image')}}</span>
                                                                                    </label>
                                                                                </div>


                                                                            </div>

                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                            <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-deljob{{$team->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}
                                                                        </h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.team',['id'=>$team->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                       {{trans('admin.do you want to')}}
                                                                        <strong> {{trans('admin.delete this member')}} </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                            @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>

                                        <div class="tab-pane" id="Department" role="tabpanel">
                                            <div style="text-align: center;">

                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom: 1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-depart"> {{trans('admin.add department')}}</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-depart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add department')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.gallery.department')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image10"
                                                                                   accept=".gif, .jpg, .png"/>
                                                                            <label for="image10">
                                                                                <span>{{trans('admin.photo')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">
                                                    <table id="datatable_tabletools1" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($gallery_departments as $gallery_department)

                                                            <tr>
                                                            <td>{{$gallery_department->name_ar}}</td>
                                                            <td>{{$gallery_department->name_en}}</td>
                                                            <td><img style="width:50px;height:45px;" src="{{asset('uploads/gallery/' . $gallery_department->img)}}"></td>

                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-depart1{{$gallery_department->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-depart2{{$gallery_department->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-depart1{{$gallery_department->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.edit')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.gallery.department',['id'=>$gallery_department->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$gallery_department->name_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                                </div>


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$gallery_department->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                                </div>

                                                                                <div class="wrap-custom-file">
                                                                                    <input type="file" name="img"
                                                                                           id="image11{{$gallery_department->id}}"
                                                                                           accept=".gif, .jpg, .png"/>
                                                                                    <label for="image11{{$gallery_department->id}}"
                                                                                           class="file-ok"
                                                                                           style="background-image: url({{url('uploads/gallery/'.$gallery_department->img)}});">
                                                                                        <span>{{trans('admin.photo')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>
                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-depart2{{$gallery_department->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}
                                                                        </h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.gallery.department',['id'=>$gallery_department->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">

                                                                        {{trans('admin.do you want to')}}
                                                                        <strong>{{trans('admin.delete this department')}} </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="Gallary" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-galls">{{trans('admin.add gallery')}}</a>

                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-galls" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add gallery')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.gallery')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>
                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.english image desc')}}</label>
                                                                            <textarea class="form-control" name="text" id="exampleFormControlTextarea1" placeholder="{{trans('admin.english image')}}" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image desc')}}</label>
                                                                            <textarea class="form-control" name="text_en" id="exampleFormControlTextarea1" placeholder="{{trans('admin.arabic image')}}" rows="3"></textarea>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="department_id" class="form-control">
                                                                                @foreach($gallery_departments as $gallery_department)
                                                                                    <option value="{{$gallery_department->id}}">{{$gallery_department->name_ar}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-g" accept=".gif, .jpg, .png" />
                                                                            <label for="image-g">
                                                                                <span>{{trans('admin.upload img')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>

                                                            <th data-hide="phone">{{trans('admin.arabic image desc')}}</th>
                                                            <th data-hide="phone">{{trans('admin.english image desc')}}</th>
                                                            <th data-hide="phone">{{trans('admin.arabic image department')}}</th>
                                                            <th data-hide="phone">{{trans('admin.english image department')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($galleries as $gallery)

                                                            <tr>
                                                            <td>{{$gallery->id}}</td>
                                                            <td>{{$gallery->name}}</td>
                                                            <td>{{$gallery->name_en }}</td>

                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-gall2{{$gallery->id}}" style="margin-top:10px;color:#fff">{{trans('admin.show')}}</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-gall5{{$gallery->id}}" style="margin-top:10px;color:#fff">{{trans('admin.show')}}</button>
                                                            </td>
                                                            <td>
                                                                {{$gallery->departments['name_ar']}}
                                                            </td>
                                                            <td>
                                                                {{$gallery->departments['name_en']}}
                                                            </td>

                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/gallery/' . $gallery->img)}}" data-img="{{asset('uploads/gallery/' . $gallery->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-gall3{{$gallery->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-gall4{{$gallery->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                            @endforeach

                                                        @foreach($galleries as $gallery)


                                                            <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-gall5{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic image desc')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$gallery->text}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-gall2{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.english image desc')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$gallery->text_en}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-gall3{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.edit')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.gallery',['id'=>$gallery->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name" value="{{$gallery->name}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$gallery->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                                </div>
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image desc')}}</label>
                                                                                    <textarea class="form-control" name="text" id="exampleFormControlTextarea1" placeholder="{{trans('admin.arabic image desc')}}" rows="3">{{$gallery->text}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.english image desc')}}</label>
                                                                                    <textarea class="form-control" name="text_en" id="exampleFormControlTextarea1" placeholder="{{trans('admin.english image desc')}}" rows="3">{{$gallery->text_en}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="department_id" class="form-control">
                                                                                        <option selected disabled>{{app()->isLocale('ar') ? $gallery->departments->name_ar: $gallery->departments->name_en}}</option>
                                                                                        @foreach($gallery_departments as $gallery_department)
                                                                                            <option value="{{$gallery_department->id}}">{{app()->isLocale('ar') ? $gallery_department->name_ar: $gallery_department->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image6-g" accept=".gif, .jpg, .png" />
                                                                                    <label for="image6-g"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/gallery/'.$gallery->img)}});" >
                                                                                        <span>{{trans('admin.upload img')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-gall4{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}
                                                                            </h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.gallery',['id'=>$gallery->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        {{trans('admin.do you want to')}}
                                                                        <strong>{{trans('admin.delete this image')}}</strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>

                                        <div class="tab-pane" id="Articles" role="tabpanel">
                                            <!-- widget div-->
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-galls-article"> {{trans('admin.add article')}}</a>

                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-galls-article" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.article')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.author in arabic')}}</label>
                                                                            <input type="text" name="author_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.author in arabic')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.author in english')}}</label>
                                                                            <input type="text" name="author_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.author in english')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.date')}}</label>
                                                                            <input type="date" name="date" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.date')}}">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic title')}}</label>
                                                                            <input type="text" name="title_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic title')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english title')}}</label>
                                                                            <input type="text" name="title_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english title')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic content')}}</label>
                                                                            <textarea class="form-control" name="content_ar" id="exampleFormControlTextarea1" placeholder="{{trans('admin.arabic content')}}" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.english content')}}</label>
                                                                            <textarea class="form-control" name="content_en" id="exampleFormControlTextarea1" placeholder="{{trans('admin.english content')}}" rows="3"></textarea>
                                                                        </div>


                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-a" accept=".gif, .jpg, .png" />
                                                                            <label for="image-a">
                                                                                <span>{{trans('admin.upload img')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="widget-body p-0">

                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.author in arabic')}}</th>
                                                            <th data-class="expand">{{trans('admin.author in english')}}</th>
                                                            <th data-hide="phone">{{trans('admin.date')}}</th>
                                                            <th data-hide="phone">{{trans('admin.arabic title')}}</th>
                                                            <th data-hide="phone">{{trans('admin.english title')}}</th>
                                                            <th data-hide="phone">{{trans('admin.arabic content')}}</th>
                                                            <th data-hide="phone">{{trans('admin.english content')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>


                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($articles as $article)

                                                        <tr>
                                                            <td>{{$article->id}}</td>
                                                            <td>{{$article->author_ar}}</td>
                                                            <td>{{$article->author_en}}</td>
                                                            <td>{{$article->date}}</td>
                                                            <td>{{$article->title_ar}}</td>
                                                            <td>{{$article->title_en}}</td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-7{{$article->id}}" style="margin-top:10px;color:#fff">{{trans('admin.show')}}</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-artic2{{$article->id}}" style="margin-top:10px;color:#fff">{{trans('admin.show')}}</button>
                                                            </td>


                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/article/' . $article->img)}}" data-img="{{asset('uploads/article/' . $article->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-6{{$article->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-5{{$article->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                        @endforeach

                                                        @foreach($articles as $article)

                                                            <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-artic2{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.english content')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$article->content_en}}</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-7{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic content')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$article->content_ar}}</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                            <!-- Modal  to Edit image-->
                                                            <div class="modal fade" id="myModal-6{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.edit')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <form action="{{route('admin.settings.edit.article',['id'=>$article->id])}}"
                                                                                  method="post" enctype="multipart/form-data">
                                                                                @csrf
                                                                                <div class="row">

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.author in arabic')}}</label>
                                                                                        <input type="text" name="author_ar" value="{{$article->author_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.author in arabic')}}">
                                                                                    </div>

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.author in english')}}</label>
                                                                                        <input type="text" name="author_en" value="{{$article->author_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.author in english')}}">
                                                                                    </div>

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.date')}}</label>
                                                                                        <input type="date" name="date" value="{{$article->date}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.date')}}">
                                                                                    </div>

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.arabic title')}}</label>
                                                                                        <input type="text" name="title_ar" value="{{$article->title_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic title')}}">
                                                                                    </div>

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.english title')}}</label>
                                                                                        <input type="text" name="title_en" value="{{$article->title_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english title')}}">
                                                                                    </div>

                                                                                    <div class="form-group col-md-12">
                                                                                        <label for="exampleFormControlTextarea1">{{trans('admin.arabic content')}}</label>
                                                                                        <textarea class="form-control" name="content_ar" id="exampleFormControlTextarea1" rows="3">{{$article->content_ar}}</textarea>
                                                                                    </div>
                                                                                    <div class="form-group col-md-12">
                                                                                        <label for="exampleFormControlTextarea1">{{trans('admin.english content')}}</label>
                                                                                        <textarea class="form-control" name="content_en" id="exampleFormControlTextarea1" rows="3">{{$article->content_en}}</textarea>
                                                                                    </div>

                                                                                    <div class="wrap-custom-file ">
                                                                                        <input type="file" name="img" id="image6-a" accept=".gif, .jpg, .png" />
                                                                                        <label for="image6-a"  class="file-ok"
                                                                                               style="background-image: url({{url('uploads/article/'.$article->img)}});" >
                                                                                            <span>{{trans('admin.upload img')}}</span>
                                                                                        </label>
                                                                                    </div>

                                                                                </div>


                                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                    <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <!-- Modal to delet Article -->
                                                        <div class="modal fade" id="myModal-5{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}
                                                                            </h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.article',['id'=>$article->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        {{trans('admin.do you want to')}} <strong>{{trans('admin.delete this article')}} </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                            @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                            <!-- end widget div -->
                                        </div>
                                        <div class="tab-pane" id="Services" role="tabpanel" style="text-align:center;">
                                            <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                               data-target="#myModal-addserv"> {{trans('admin.add service')}}</a>
                                            <!-- Modal  to Edit image-->
                                            <div class="modal fade" id="myModal-addserv" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}
                                                                </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body" style="text-align:left;">
                                                            <form action="{{route('admin.settings.add.service')}}"
                                                                  method="post" enctype="multipart/form-data">
                                                                @csrf
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                    <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                    <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlTextarea1">
                                                                        {{trans('admin.arabic content')}}</label>
                                                                    <textarea class="form-control" name="text_ar" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlTextarea1">
                                                                        {{trans('admin.english content')}}</label>
                                                                    <textarea class="form-control" name="text_en" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                                </div>
                                                                <div class="wrap-custom-file ">
                                                                    <input type="file" name="img" id="image5-s" accept=".gif, .jpg, .png" />
                                                                    <label for="image5-s">
                                                                        <span><i class="fa fa-file-image-o " style="font-size:5rem;"></i></span>
                                                                    </label>
                                                                </div>

                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                    <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                <thead>
                                                <tr>
                                                    <th data-hide="phone">{{trans('admin.id')}}</th>
                                                    <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                    <th data-class="expand">{{trans('admin.english name')}}</th>


                                                    <th data-hide="phone">{{trans('admin.arabic content')}}</th>
                                                    <th data-hide="phone">{{trans('admin.english content')}}</th>
                                                    <th data-hide="phone">{{trans('admin.image')}}</th>

                                                    <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($services as $service)
                                                <tr>

                                                    <td>{{$service->id}}</td>

                                                    <td>{{$service->name_ar}}</td>
                                                    <td>{{$service->name_en}}</td>
                                                    <td>
                                                        <button class="btn btn-info" data-toggle="modal" data-target="#myModel-ar{{$service->id}}" style="margin-top:10px;color:#fff">{{trans('admin.show')}}</button>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-info" data-toggle="modal" data-target="#myModel-en{{$service->id}}" style="margin-top:10px;color:#fff">{{trans('admin.show')}}</button>
                                                    </td>
                                                    <td>
                                                        <div class="superbox-list superbox-8">
                                                            <img src="{{asset('uploads/services/' . $service->img)}}" data-img="{{asset('uploads/service/' . $service->img)}}"
                                                                 alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                        </div>
                                                    </td>


                                                    <td>
                                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModel-2{{$service->id}}" style="margin-bottom:10px;"><i
                                                                    class="fa fa-edit"></i></button>
                                                        <button class="btn btn-danger" data-toggle="modal" data-target="#myModel-1{{$service->id}}" style="margin-bottom:10px;"><i
                                                                    class="fa fa-trash"></i></button>

                                                    </td>

                                                </tr>

                                                @endforeach

                                                @foreach($services as $service)

                                                    <!-- Modal  to show image-->
                                                <div class="modal fade" id="myModel-ar{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                    {{trans('admin.arabic content')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <p> {{$service->text_ar}}</p>
                                                            </div>
                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Modal  to show image-->
                                                <div class="modal fade" id="myModel-en{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                    {{trans('admin.english content')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <p> {{$service->text_en}}</p>
                                                            </div>
                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Modal  to Edit image-->
                                                <div class="modal fade" id="myModel-2{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.edit')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.edit.service',['id'=>$service->id])}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                        <input type="text" name="name_ar" value="{{$service->name_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                        <input type="text" name="name_en" value="{{$service->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleFormControlTextarea1">
                                                                            {{trans('admin.arabic content')}}</label>
                                                                        <textarea class="form-control" name="text_ar" id="exampleFormControlTextarea1" rows="3">{{$service->text_ar}}</textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleFormControlTextarea1">
                                                                            {{trans('admin.english content')}}</label>
                                                                        <textarea class="form-control" name="text_en" id="exampleFormControlTextarea1" rows="3">{{$service->text_en}}</textarea>
                                                                    </div>
                                                                    <div class="wrap-custom-file ">
                                                                        <input type="file" name="img" id="image5-s{{$service->id}}" accept=".gif, .jpg, .png" />
                                                                        <label for="image5-s{{$service->id}}" class="file-ok"
                                                                               style="background-image: url({{url('uploads/services/'.$service->img)}});">
                                                                            <span><i class="fa fa-file-image-o " style="font-size:5rem;"></i></span>
                                                                        </label>
                                                                    </div>

                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>



                                                <!-- Modal to delet image -->
                                                <div class="modal fade" id="myModel-1{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form action="{{route('admin.settings.delete.service',['id'=>$service->id])}}"
                                                                  method="post">
                                                                @csrf
                                                            <div class="modal-body" style="text-align:left;">
                                                                {{trans('admin.do you want to')}}
                                                                <strong>{{trans('admin.delete this service')}} </strong> ?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="Time" role="tabpanel">
                                            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%" style="margin-top:1rem">
                                                <thead>
                                                @foreach($work_days as $day)
                                                <tr>
                                                    <th>{{trans('admin.day')}}</th>
                                                    <th>{{trans('admin.time')}}</th>


                                                    <th>{{trans('admin.status')}}</th>


                                                    <th>{{trans('admin.action')}}</th>

                                                </tr>
                                                </thead>
                                                <tbody>


                                                <tr>
                                                    <td>{{$day->day}}</td>
                                                    <td> {{$day->from}} {{$day->from_timing}}
                                                        : {{$day->to}} {{$day->to_timing}}</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select class="form-control status"
                                                                    uid="{{ $day->id }}">
                                                                <option @if($day->is_active) selected
                                                                        @endif value="1">
                                                                    {{trans('admin.active')}}
                                                                </option>
                                                                <option @if(!$day->is_active) selected
                                                                        @endif value="0">
                                                                    {{trans('admin.pending')}}
                                                                </option>
                                                            </select>

                                                        </div>
                                                    </td>

                                                    <td>
                                                        <button class="btn btn-primary" type="button"
                                                                data-toggle="modal"
                                                                data-target="#exampleModalCenter{{$day->id}}"
                                                                style="margin-bottom:10px;"><i
                                                                    class="fa fa-edit"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @endforeach

                                                @foreach($work_days as $day)

                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalCenter{{$day->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('admin.edit')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form action="{{route('admin.settings.edit.time',['id'=>$day->id])}}"
                                                                  method="post" enctype="multipart/form-data">
                                                                @csrf
                                                            <div class="modal-body">

                                                                <div class="row">

                                                                    <div class="col-lg-6 col-sm-12 col-xs-12 ">
                                                                        <h5>{{trans('admin.from')}}:</h5>
                                                                        <div style="float:left" class="form-group">

                                                                            <select name="from" class="form-control" id="exampleFormControlSelect1">
                                                                                <option value="1" @if ($day->from == 1)
                                                                                selected
                                                                                        @endif>1
                                                                                </option>
                                                                                <option value="2" @if ($day->from == 2)
                                                                                selected
                                                                                        @endif>2
                                                                                </option>
                                                                                <option value="3" @if ($day->from == 3)
                                                                                selected
                                                                                        @endif>3
                                                                                </option>
                                                                                <option value="4" @if ($day->from == 4)
                                                                                selected
                                                                                        @endif>4
                                                                                </option>
                                                                                <option value="5" @if ($day->from == 5)
                                                                                selected
                                                                                        @endif>5
                                                                                </option>
                                                                                <option value="6" @if ($day->from == 6)
                                                                                selected
                                                                                        @endif>6
                                                                                </option>
                                                                                <option value="7" @if ($day->from == 7)
                                                                                selected
                                                                                        @endif>7
                                                                                </option>
                                                                                <option value="8" @if ($day->from == 8)
                                                                                selected
                                                                                        @endif>8
                                                                                </option>
                                                                                <option value="9" @if ($day->from == 9)
                                                                                selected
                                                                                        @endif>9
                                                                                </option>
                                                                                <option value="10" @if ($day->from == 10)
                                                                                selected
                                                                                        @endif>10
                                                                                </option>
                                                                                <option value="11" @if ($day->from == 11)
                                                                                selected
                                                                                        @endif>11
                                                                                </option>
                                                                                <option value="12" @if ($day->from == 12)
                                                                                selected
                                                                                        @endif>12
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                        <div style="display:inline-block;margin-left: 20px;" class="form-group">

                                                                            <select name="from_timing" class="form-control" id="exampleFormControlSelect1">
                                                                                <option value="am"
                                                                                        @if ($day->from_timing == 'am')
                                                                                        selected
                                                                                        @endif>{{trans('admin.am')}}
                                                                                </option>
                                                                                <option value="pm"
                                                                                        @if ($day->from_timing == 'pm')
                                                                                        selected
                                                                                        @endif>{{trans('admin.pm')}}
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6 col-sm-12 col-xs-12">
                                                                        <h5>{{trans('admin.to')}}:</h5>
                                                                        <div style="float:left" class="form-group">

                                                                            <select name="to" class="form-control" id="exampleFormControlSelect1">
                                                                                <option value="1" @if ($day->to == 1)
                                                                                selected
                                                                                        @endif>1
                                                                                </option>
                                                                                <option value="2" @if ($day->to == 2)
                                                                                selected
                                                                                        @endif>2
                                                                                </option>
                                                                                <option value="3" @if ($day->to == 3)
                                                                                selected
                                                                                        @endif>3
                                                                                </option>
                                                                                <option value="4" @if ($day->to == 4)
                                                                                selected
                                                                                        @endif>4
                                                                                </option>
                                                                                <option value="5" @if ($day->to == 5)
                                                                                selected
                                                                                        @endif>5
                                                                                </option>
                                                                                <option value="6" @if ($day->to == 6)
                                                                                selected
                                                                                        @endif>6
                                                                                </option>
                                                                                <option value="7" @if ($day->to == 7)
                                                                                selected
                                                                                        @endif>7
                                                                                </option>
                                                                                <option value="8" @if ($day->to == 8)
                                                                                selected
                                                                                        @endif>8
                                                                                </option>
                                                                                <option value="9" @if ($day->to == 9)
                                                                                selected
                                                                                        @endif>9
                                                                                </option>
                                                                                <option value="10" @if ($day->to == 10)
                                                                                selected
                                                                                        @endif>10
                                                                                </option>
                                                                                <option value="11" @if ($day->to == 11)
                                                                                selected
                                                                                        @endif>11
                                                                                </option>
                                                                                <option value="12" @if ($day->to == 12)
                                                                                selected
                                                                                        @endif>12
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                        <div style="display:inline-block;margin-left: 20px;" class="form-group">

                                                                            <select name="to_timing" class="form-control" id="exampleFormControlSelect1">
                                                                                <option value="am"
                                                                                        @if ($day->to_timing == 'am')
                                                                                        selected
                                                                                        @endif>{{trans('admin.am')}}
                                                                                </option>
                                                                                <option value="pm"
                                                                                        @if ($day->to_timing == 'pm')
                                                                                        selected
                                                                                        @endif>{{trans('admin.pm')}}
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>


                                        <div class="tab-pane" id="Country" role="tabpanel">
                                            <!-- widget div-->
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;margin-bottom: 1rem;" href="javascript:void(0);"
                                                   class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-coun"> {{trans('admin.add country')}}</a>



                                                <!-- Modal  to add supervisor-->
                                                <div class="modal fade" id="myModal-coun" tabindex="-1" role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel"
                                                                    style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.country')}}"
                                                                      method="post">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                        <input name="name_ar" type="text" class="form-control"
                                                                               id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                        <input name="name_en" type="text" class="form-control"
                                                                               id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default"
                                                                                data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit"
                                                                                class="btn btn-primary">{{trans('admin.save changes')}}
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools"
                                                           class="table table-striped table-bordered table-hover"
                                                           width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand"> {{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand"> {{trans('admin.english name')}}</th>

                                                            <th data-hide="phone">{{trans('admin.province')}}</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($countries as $country)

                                                        <tr>
                                                            <td>{{$country->id}}</td>
                                                            <td>{{$country->name_ar}}</td>
                                                            <td>{{$country->name_en}}</td>
                                                            <td>
                                                                <a href="{{route('admin.settings.provinces',['id'=>$country->id])}}">{{trans('admin.show')}}</a>
                                                            </td>

                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal"
                                                                        data-target="#myModal-2{{$country->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal"
                                                                        data-target="#myModal-3{{$country->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        @endforeach

                                                        @foreach($countries as $country)


                                                            <!-- Modal  to Edit supervisor-->
                                                        <div class="modal fade" id="myModal-2{{$country->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"
                                                                            id="exampleModalLabel"
                                                                            style="font-weight:bold">{{trans('admin.edit')}}</h5>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.edit.country',['id'=>$country->id])}}"
                                                                          method="post" enctype="multipart/form-data">
                                                                        @csrf
                                                                    <div class="modal-body"
                                                                         style="text-align:left;">
                                                                            <div class="form-group">
                                                                                <label
                                                                                        for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                <input type="text" name="name_ar" value="{{$country->name_ar}}"
                                                                                       class="form-control"
                                                                                       id="exampleInputText1">
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                        for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                <input type="text" name="name_en" value="{{$country->name_en}}"
                                                                                       class="form-control"
                                                                                       id="exampleInputText1">
                                                                            </div>

                                                                            <div class="modal-footer"
                                                                                 style="margin-top:1rem;">
                                                                                <button type="button"
                                                                                        class="btn btn-default"
                                                                                        data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit"
                                                                                        class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                    </div>
                                                                        </form>
                                                                    </div>

                                                            </div>
                                                        </div>

                                                        <!-- Modal to delet supervisor -->
                                                        <div class="modal fade" id="myModal-3{{$country->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"
                                                                            id="exampleModalLabel"
                                                                            style="font-weight:bold">{{trans('admin.delete')}}
                                                                            </h5>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.country',['id'=>$country->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body"
                                                                         style="text-align:left;">
                                                                        {{trans('admin.do you want to')}} <strong>{{trans('admin.delete this country')}} </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button"
                                                                                class="btn btn-default"
                                                                                data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                        <button type="submit"
                                                                                class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                            <!-- end widget div -->
                                        </div>

                                        <div class="tab-pane" id="Color" role="tabpanel">
                                            <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-3"
                                                 data-widget-editbutton="false" >
                                                <header>
                                                    <div class="widget-header">
																<span class="widget-icon"> <i class="fa fa-lg  fa-globe"></i>
																</span>
                                                        <h2>{{trans('admin.web slider')}}</h2>
                                                    </div>
                                                </header>
                                                <!-- widget div-->
                                                <div style="text-align: center;">

                                                    <!-- widget content -->
                                                    <div class="widget-body p-0">


                                                        <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th data-hide="phone">{{trans('admin.id')}}</th>
                                                                <th data-class="expand">{{trans('admin.first color')}}</th>
                                                                <th data-class="expand">{{trans('admin.second color')}}</th>

                                                                <th data-class="expand">{{trans('admin.third color')}}</th>
                                                                <th data-class="expand">{{trans('admin.fourth color')}}</th>

                                                                <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @foreach($web_colors as $web_color)

                                                            <tr>

                                                                <td>{{$web_color->id}}</td>
                                                                <td>{{$web_color->first}}</td>
                                                                <td>{{$web_color->second}}</td>
                                                                <td>{{$web_color->third}}</td>
                                                                <td>{{$web_color->fourth}}</td>

                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-job{{$web_color->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-edit"></i></button>

                                                                </td>

                                                            </tr>

                                                            @endforeach

                                                            @foreach($web_colors as $web_color)

                                                            <!-- Modal  to Edit image-->
                                                            <div class="modal fade" id="myModal-job{{$web_color->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                                {{trans('admin.edit')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <form action="{{route('admin.settings.edit.web.color',['id'=>$web_color->id])}}"
                                                                                  method="post" enctype="multipart/form-data">
                                                                                @csrf
                                                                                <div class="row">


                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.first color')}}</label>
                                                                                        <input type="text" name="first" value="{{$web_color->first}}" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.second color')}}</label>
                                                                                        <input type="text" name="second" value="{{$web_color->second}}" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.third color')}}</label>
                                                                                        <input type="text" name="third" value="{{$web_color->third}}" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.fourth color')}}</label>
                                                                                        <input type="text" name="fourth" value="{{$web_color->fourth}}" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                                    </div>
                                                                                </div>

                                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                    <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                                @endforeach

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                    <!-- end widget content -->

                                                </div>
                                                <!-- end widget div -->

                                            </div>
                                            <!-- end widget -->
                                            <!-- Widget ID (each widget will need unique ID)-->
                                            <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-4"
                                                 data-widget-editbutton="false">
                                                <header>
                                                    <div class="widget-header">
					<span class="widget-icon"> <i class="fa fa-lg  fa-globe"></i>
					</span>
                                                        <h2>{{trans('admin.mobile slider')}}</h2>
                                                    </div>
                                                </header>
                                                <!-- widget div-->
                                                <div style="text-align: center;">

                                                    <!-- widget content -->
                                                    <div class="widget-body p-0">


                                                        <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th data-hide="phone">{{trans('admin.id')}}</th>
                                                                <th data-class="expand">{{trans('admin.first color')}}</th>
                                                                <th data-class="expand">{{trans('admin.second color')}}</th>

                                                                <th data-class="expand">{{trans('admin.third color')}}</th>
                                                                <th data-class="expand">{{trans('admin.fourth color')}}</th>

                                                                <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody>


                                                            @foreach($mobile_colors as $mobile_color)

                                                                <tr>

                                                                    <td>{{$mobile_color->id}}</td>
                                                                    <td>{{$mobile_color->first}}</td>
                                                                    <td>{{$mobile_color->second}}</td>
                                                                    <td>{{$mobile_color->third}}</td>
                                                                    <td>{{$mobile_color->fourth}}</td>

                                                                    <td>
                                                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-job-mob{{$mobile_color->id}}" style="margin-bottom:10px;"><i
                                                                                    class="fa fa-edit"></i></button>

                                                                    </td>

                                                                </tr>

                                                            @endforeach

                                                            @foreach($mobile_colors as $mobile_color)


                                                            <!-- Modal  to Edit image-->
                                                            <div class="modal fade" id="myModal-job-mob{{$mobile_color->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                                {{trans('admin.edit')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <form action="{{route('admin.settings.edit.mobile.color',['id'=>$mobile_color->id])}}"
                                                                                  method="post" enctype="multipart/form-data">
                                                                                @csrf
                                                                                <div class="row">
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.first color')}}</label>
                                                                                        <input type="text" name="first" value="{{$mobile_color->first}}" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.second color')}}</label>
                                                                                        <input type="text" name="second" value="{{$mobile_color->second}}" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.third color')}}</label>
                                                                                        <input type="text" name="third" value="{{$mobile_color->third}}" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.fourth color')}}</label>
                                                                                        <input type="text" name="fourth" value="{{$mobile_color->fourth}}" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                                    </div>
                                                                                </div>

                                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                    <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            @endforeach

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                    <!-- end widget content -->

                                                </div>
                                                <!-- end widget div -->

                                            </div>
                                            <!-- end widget -->
                                        </div>
                                        <div class="tab-pane" id="Web" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-1-web"> {{trans('admin.add slider image')}}</a>

                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-1-web" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.web.slider')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1"> {{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1"> {{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>


                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image9-slid" accept=".gif, .jpg, .png" />
                                                                            <label for="image9-slid">
                                                                                <span>{{trans('admin.image')}}</span>
                                                                            </label>
                                                                        </div>


                                                                    </div>

                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>

                                                            <th data-hide="phone">{{trans('admin.image')}}</th>
                                                            <th data-hide="phone">{{trans('admin.status')}}</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($web_sliders as $web_slider)


                                                            <tr>

                                                            <td>{{$web_slider->id}}</td>
                                                            <td>{{$web_slider->name_ar}}</td>
                                                            <td>{{$web_slider->name_en}}</td>



                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/slider/' . $web_slider->img)}}" data-img="{{asset('uploads/sliders/' . $web_slider->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="form-group">
                                                                    <select class="form-control status"
                                                                            uid="{{ $web_slider->id }}">
                                                                        <option @if($web_slider->is_active) selected
                                                                                @endif value="1">
                                                                            {{trans('admin.active')}}
                                                                        </option>
                                                                        <option @if(!$web_slider->is_active) selected
                                                                                @endif value="0">
                                                                            {{trans('admin.pending')}}
                                                                        </option>
                                                                    </select>

                                                                </div>
                                                            </td>

                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-job-web{{$web_slider->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-deljob-web{{$web_slider->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                            @endforeach

                                                        @foreach($web_sliders as $web_slider)

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-job-web{{$web_slider->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.edit')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.web.slider',['id'=>$web_slider->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$web_slider->name_ar}}" class="form-control" id="exampleInputText1">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$web_slider->name_en}}" class="form-control" id="exampleInputText1">
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image9-web" accept=".gif, .jpg, .png" />
                                                                                    <label for="image9-web" class="file-ok"
                                                                                           style="background-image: url({{url('uploads/slider/'.$web_slider->img)}});">
                                                                                        <span>{{trans('admin.image')}}</span>
                                                                                    </label>
                                                                                </div>


                                                                            </div>

                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-deljob-web{{$web_slider->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}
                                                                        </h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.web.slider',['id'=>$web_slider->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        {{trans('admin.do you want to')}}
                                                                        <strong>{{trans('admin.delete this image')}}</strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                            <!-- end widget div -->
                                        </div>
                                        <div class="tab-pane" id="Mobile" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-mob"> {{trans('admin.add slider image')}}</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-mob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.mobile.slider')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1"> {{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1"> {{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>


                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image9-slider" accept=".gif, .jpg, .png" />
                                                                            <label for="image9-slider">
                                                                                <span>{{trans('admin.image')}}</span>
                                                                            </label>
                                                                        </div>


                                                                    </div>

                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>

                                                            <th data-hide="phone">{{trans('admin.image')}}</th>
                                                            <th data-hide="phone">{{trans('admin.status')}}</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($mobile_sliders as $mobile_slider)

                                                        <tr>

                                                            <td>{{$mobile_slider->id}}</td>
                                                            <td>{{$mobile_slider->name_ar}}</td>
                                                            <td>{{$mobile_slider->name_en}}</td>



                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/slider/' . $mobile_slider->img)}}" data-img="{{asset('uploads/slider/' . $mobile_slider->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="form-group">
                                                                    <select class="form-control status"
                                                                            uid="{{ $mobile_slider->id }}">
                                                                        <option @if($mobile_slider->is_active) selected
                                                                                @endif value="1">
                                                                            {{trans('admin.active')}}
                                                                        </option>
                                                                        <option @if(!$mobile_slider->is_active) selected
                                                                                @endif value="0">
                                                                            {{trans('admin.pending')}}
                                                                        </option>
                                                                    </select>

                                                                </div>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-job-mob{{$mobile_slider->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-deljob-mob{{$mobile_slider->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        @endforeach

                                                        @foreach($mobile_sliders as $mobile_slider)

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-job-mob{{$mobile_slider->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.edit')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.mobile.slider',['id'=>$web_slider->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$mobile_slider->name_ar}}" class="form-control" id="exampleInputText1">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$mobile_slider->name_en}}" class="form-control" id="exampleInputText1">
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image9-web" accept=".gif, .jpg, .png" />
                                                                                    <label for="image9-web" class="file-ok"
                                                                                           style="background-image: url({{url('uploads/slider/'.$mobile_slider->img)}});">
                                                                                        <span>{{trans('admin.image')}}</span>
                                                                                    </label>
                                                                                </div>


                                                                            </div>

                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-deljob-mob{{$mobile_slider->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}
                                                                        </h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.mobile.slider',['id'=>$web_slider->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        {{trans('admin.do you want to')}}
                                                                        <strong>{{trans('admin.delete this image')}} </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="Testimonial" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-tgalls"> {{trans('admin.add testmonials')}}</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-tgalls" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.testmonials')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1"> {{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="Image Name In Arabic">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1"> {{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="Image Name In English">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1"> {{trans('admin.arabic say')}}</label>
                                                                            <textarea type="text" name="say_ar" class="form-control" id="exampleInputText1"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1"> {{trans('admin.english say')}}</label>
                                                                            <textarea type="text" name="say_en" class="form-control" id="exampleInputText1"></textarea>
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image9-test" accept=".gif, .jpg, .png" />
                                                                            <label for="image9-test">
                                                                                <span>{{trans('admin.image')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic say')}}</th>
                                                            <th data-class="expand">{{trans('admin.english say')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>
                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($testmonials as $testmonial)


                                                            <tr>

                                                            <td>{{$testmonial->id}}</td>
                                                            <td>{{$testmonial->name_ar}}</td>
                                                                <td>{{$testmonial->name_en}}</td>

                                                                <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-tgall5-ar{{$testmonial->id}}" style="margin-top:10px;color:#fff">{{trans('admin.show')}}</button>
                                                            </td>

                                                                <td>
                                                                    <button class="btn btn-info" data-toggle="modal" data-target="#myModal-tgall2-en{{$testmonial->id}}" style="margin-top:10px;color:#fff">{{trans('admin.show')}}</button>
                                                                </td>
                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/testmonials/' . $testmonial->img)}}" data-img="{{asset('uploads/testmonials/' . $testmonial->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-tgall3{{$testmonial->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-tgall4{{$testmonial->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                        @endforeach

                                                        @foreach($testmonials as $testmonial)

                                                            <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-tgall5-ar{{$testmonial->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic say')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$testmonial->say_ar}}</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-tgall2-en{{$testmonial->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.english say')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$testmonial->say_en}}</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-tgall3{{$testmonial->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.edit')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.testmonials',['id'=>$testmonial->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$testmonial->name_ar}}" class="form-control" id="exampleInputText1">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$testmonial->name_en}}" class="form-control" id="exampleInputText1">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic say')}}</label>
                                                                                    <textarea type="text" name="say_ar" class="form-control" id="exampleInputText1">{{$testmonial->say_ar}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english say')}}</label>
                                                                                    <textarea type="text" name="say_en" class="form-control" id="exampleInputText1">{{$testmonial->say_en}}</textarea>
                                                                                </div>


                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image9-web" accept=".gif, .jpg, .png" />
                                                                                    <label for="image9-web" class="file-ok"
                                                                                           style="background-image: url({{url('uploads/testmonials/'.$testmonial->img)}});">
                                                                                        <span>{{trans('admin.image')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-tgall4{{$testmonial->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.testmonials',['id'=>$testmonial->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        {{trans('admin.do you want to')}}
                                                                        <strong>{{trans('admin.delete this say')}} </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                            @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="clogo" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-cgalls"> {{trans('admin.add image')}}</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-cgalls" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.customer.logo')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>
                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image6-clogo" accept=".gif, .jpg, .png" />
                                                                            <label for="image6-clogo">
                                                                                <span>{{trans('admin.upload img')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>
                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($customer_logos as $customer_logo)

                                                        <tr>

                                                            <td>{{$customer_logo->id}}</td>
                                                            <td>{{$customer_logo->name_ar}}</td>
                                                            <td>{{$customer_logo->name_en}}</td>

                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/logo/' . $customer_logo->img)}}" data-img="{{asset('uploads/logo/' . $customer_logo->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-cgall3{{$customer_logo->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-cgall4{{$customer_logo->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-cgall3{{$customer_logo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.edit')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.customer.logo',['id'=>$customer_logo->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$customer_logo->name_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$customer_logo->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image9-clogo" accept=".gif, .jpg, .png" />
                                                                                    <label for="image9-clogo" class="file-ok"
                                                                                           style="background-image: url({{url('uploads/logo/'.$customer_logo->img)}});">
                                                                                        <span>{{trans('admin.image')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-cgall4{{$customer_logo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.customer.logo',['id'=>$customer_logo->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        {{trans('admin.do you want to')}}
                                                                        <strong>{{trans('admin.delete this logo')}} </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="conditions" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-cond"> {{trans('admin.add condition')}}</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-cond" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.condition')}}"
                                                                      method="post">
                                                                    @csrf
                                                                    <div class="row">

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($conditions as $condition)

                                                            <tr>

                                                                <td>{{$condition->id}}</td>
                                                                <td>{{$condition->name_ar}}</td>
                                                                <td>{{$condition->name_en}}</td>

                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-cond{{$condition->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-edit"></i></button>
                                                                    <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-cond{{$condition->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-trash"></i></button>

                                                                </td>

                                                            </tr>

                                                            <!-- Modal  to Edit image-->
                                                            <div class="modal fade" id="myModal-cond{{$condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <form action="{{route('admin.settings.edit.condition',['id'=>$condition->id])}}"
                                                                                  method="post">
                                                                                @csrf
                                                                                <div class="row">


                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                        <input type="text" name="name_ar" value="{{$condition->name_ar}}" class="form-control" id="exampleInputText1">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                        <input type="text" name="name_en" value="{{$condition->name_en}}" class="form-control" id="exampleInputText1">
                                                                                    </div>

                                                                                </div>


                                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                    <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <!-- Modal to delet image -->
                                                            <div class="modal fade" id="myModal-cond{{$condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <form action="{{route('admin.settings.delete.condition',['id'=>$condition->id])}}"
                                                                              method="post">
                                                                            @csrf
                                                                            <div class="modal-body" style="text-align:left;">
                                                                                {{trans('admin.do you want to')}}
                                                                                <strong>{{trans('admin.delete this condition')}} </strong> ?
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="sh-conditions" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-shcond"> {{trans('admin.add condition')}}</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-shcond" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.sh.condition')}}"
                                                                      method="post">
                                                                    @csrf
                                                                    <div class="row">

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($shipping_conditions as $shipping_condition)

                                                            <tr>

                                                                <td>{{$shipping_condition->id}}</td>
                                                                <td>{{$shipping_condition->name_ar}}</td>
                                                                <td>{{$shipping_condition->name_en}}</td>

                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-shcond{{$shipping_condition->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-edit"></i></button>
                                                                    <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-dshcond{{$shipping_condition->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-trash"></i></button>

                                                                </td>

                                                            </tr>

                                                            <!-- Modal  to Edit image-->
                                                            <div class="modal fade" id="myModal-shcond{{$shipping_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <form action="{{route('admin.settings.edit.sh.condition',['id'=>$shipping_condition->id])}}"
                                                                                  method="post">
                                                                                @csrf
                                                                                <div class="row">


                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                        <input type="text" name="name_ar" value="{{$shipping_condition->name_ar}}" class="form-control" id="exampleInputText1">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                        <input type="text" name="name_en" value="{{$shipping_condition->name_en}}" class="form-control" id="exampleInputText1">
                                                                                    </div>

                                                                                </div>


                                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                    <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <!-- Modal to delet image -->
                                                            <div class="modal fade" id="myModal-dshcond{{$shipping_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <form action="{{route('admin.settings.delete.condition',['id'=>$shipping_condition->id])}}"
                                                                              method="post">
                                                                            @csrf
                                                                            <div class="modal-body" style="text-align:left;">
                                                                                {{trans('admin.do you want to')}}
                                                                                <strong>{{trans('admin.delete this condition')}} </strong> ?
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="questions" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-ques"> {{trans('admin.add question')}}</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-ques" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.question')}}"
                                                                      method="post">
                                                                    @csrf
                                                                    <div class="row">

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic question')}}</label>
                                                                            <input type="text" name="question_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic question')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english question')}}</label>
                                                                            <input type="text" name="question_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english question')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic answer')}}</label>
                                                                            <textarea type="text" name="answer_ar" class="form-control" id="exampleInputText1"></textarea>
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english answer')}}</label>
                                                                            <textarea type="text" name="answer_en" class="form-control" id="exampleInputText1"></textarea>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic question')}}</th>
                                                            <th data-class="expand">{{trans('admin.english question')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic answer')}}</th>
                                                            <th data-class="expand">{{trans('admin.english answer')}}</th>
                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($questions as $question)

                                                            <tr>

                                                                <td>{{$question->id}}</td>
                                                                <td>{{$question->question_ar}}</td>
                                                                <td>{{$question->question_en}}</td>
                                                                <td>{{$question->answer_ar}}</td>
                                                                <td>{{$question->answer_en}}</td>

                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-ques{{$question->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-edit"></i></button>
                                                                    <button class="btn btn-danger" data-toggle="modal" data-target="#myModald-ques{{$question->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-trash"></i></button>

                                                                </td>

                                                            </tr>

                                                            <!-- Modal  to Edit image-->
                                                            <div class="modal fade" id="myModal-ques{{$question->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <form action="{{route('admin.settings.edit.question',['id'=>$question->id])}}"
                                                                                  method="post">
                                                                                @csrf
                                                                                <div class="row">


                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.arabic question')}}</label>
                                                                                        <input type="text" name="question_ar" value="{{$question->question_ar}}" class="form-control" id="exampleInputText1">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.english question')}}</label>
                                                                                        <input type="text" name="question_en" value="{{$question->question_en}}" class="form-control" id="exampleInputText1">
                                                                                    </div>

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.arabic answer')}}</label>
                                                                                        <textarea type="text" name="answer_ar" class="form-control" id="exampleInputText1">{{$question->answer_ar}}</textarea>
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.english answer')}}</label>
                                                                                        <textarea type="text" name="answer_en" class="form-control" id="exampleInputText1">{{$question->answer_en}}</textarea>
                                                                                    </div>

                                                                                </div>


                                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                    <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <!-- Modal to delet image -->
                                                            <div class="modal fade" id="myModald-ques{{$question->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <form action="{{route('admin.settings.delete.question',['id'=>$question->id])}}"
                                                                              method="post">
                                                                            @csrf
                                                                            <div class="modal-body" style="text-align:left;">
                                                                                {{trans('admin.do you want to')}}
                                                                                <strong>{{trans('admin.delete this question')}} </strong> ?
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="info" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-info"> {{trans('admin.add info')}}</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.info')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">
                                                                                {{trans('admin.arabic content')}}</label>
                                                                            <textarea class="form-control" name="desc_ar" id="exampleFormControlTextarea1"rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">
                                                                                {{trans('admin.english content')}}</label>
                                                                            <textarea class="form-control" name="desc_en" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image6-info" accept=".gif, .jpg, .png" />
                                                                            <label for="image6-info">
                                                                                <span>{{trans('admin.upload img')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-hide="phone">{{trans('admin.arabic content')}}</th>
                                                            <th data-hide="phone">{{trans('admin.english content')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>
                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($info as $inf)

                                                            <tr>

                                                                <td>{{$inf->id}}</td>
                                                                <td>{{$inf->name_ar}}</td>
                                                                <td>{{$inf->name_en}}</td>
                                                                <td>
                                                                    <button class="btn btn-info" data-toggle="modal" data-target="#myModel-infoar{{$inf->id}}" style="margin-top:10px;color:#fff">{{trans('admin.show')}}</button>
                                                                </td>
                                                                <td>
                                                                    <button class="btn btn-info" data-toggle="modal" data-target="#myModel-infoen{{$inf->id}}" style="margin-top:10px;color:#fff">{{trans('admin.show')}}</button>
                                                                </td>

                                                                <td>
                                                                    <div class="superbox-list superbox-8">
                                                                        <img src="{{asset('uploads/info/' . $inf->img)}}" data-img="{{asset('uploads/info/' . $inf->img)}}"
                                                                             alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                    </div>
                                                                </td>


                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-info{{$inf->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-edit"></i></button>
                                                                    <button class="btn btn-danger" data-toggle="modal" data-target="#myModald-info{{$inf->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-trash"></i></button>

                                                                </td>

                                                            </tr>

                                                        @endforeach

                                                        @foreach($info as $inf)


                                                            <!-- Modal  to show image-->
                                                            <div class="modal fade" id="myModel-infoen{{$inf->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                                {{trans('admin.english content')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <p> {{$inf->desc_en}}</p>
                                                                        </div>
                                                                        <div class="modal-footer" style="margin-top:1rem;">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="modal fade" id="myModel-infoar{{$inf->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                                {{trans('admin.arabic content')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <p> {{$inf->desc_ar}}</p>
                                                                        </div>
                                                                        <div class="modal-footer" style="margin-top:1rem;">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- Modal  to Edit image-->
                                                            <div class="modal fade" id="myModal-info{{$inf->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <form action="{{route('admin.settings.edit.info',['id'=>$inf->id])}}"
                                                                                  method="post" enctype="multipart/form-data">
                                                                                @csrf
                                                                                <div class="row">


                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                        <input type="text" name="name_ar" value="{{$inf->name_ar}}" class="form-control" id="exampleInputText1">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                        <input type="text" name="name_en" value="{{$inf->name_en}}" class="form-control" id="exampleInputText1">
                                                                                    </div>

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleFormControlTextarea1">
                                                                                            {{trans('admin.arabic content')}}</label>
                                                                                        <textarea class="form-control" name="desc_ar" id="exampleFormControlTextarea1" rows="3">{{$inf->desc_ar}}</textarea>
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleFormControlTextarea1">
                                                                                            {{trans('admin.english content')}}</label>
                                                                                        <textarea class="form-control" name="desc_en" id="exampleFormControlTextarea1" rows="3">{{$inf->desc_en}}</textarea>
                                                                                    </div>

                                                                                    <div class="wrap-custom-file ">
                                                                                        <input type="file" name="img" id="image9-$inf" accept=".gif, .jpg, .png" />
                                                                                        <label for="image9-$inf" class="file-ok"
                                                                                               style="background-image: url({{url('uploads/logo/'.$customer_logo->img)}});">
                                                                                            <span>{{trans('admin.image')}}</span>
                                                                                        </label>
                                                                                    </div>

                                                                                </div>


                                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                    <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <!-- Modal to delet image -->
                                                            <div class="modal fade" id="myModald-info{{$inf->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">{{trans('admin.delete')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <form action="{{route('admin.settings.delete.info',['id'=>$inf->id])}}"
                                                                              method="post">
                                                                            @csrf
                                                                            <div class="modal-body" style="text-align:left;">
                                                                                {{trans('admin.do you want to')}}
                                                                                <strong>{{trans('admin.delete this info')}} </strong> ?
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- end row -->

                        <!-- end row -->

                    </section>
                    <!-- end widget grid -->
                </div>
            </div>
        </div>

    </div>
@stop

@section('scripts')
    <script>
        $('#tabs').tabs();
        // Dynamic tabs
        var tabTitle = $("#tab_title"), tabContent = $("#tab_content"),
            tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:5px; left:5px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>",
            tabCounter = 2;

        var tabs = $("#tabs2").tabs();

        // modal dialog init: custom buttons and a "close" callback reseting the form inside
        var dialog = $("#addtab").dialog({
            autoOpen: false,
            width: 600,
            resizable: false,
            modal: true,
            buttons: [{
                html: "<i class='fa fa-times'></i>&nbsp; Cancel",
                "class": "btn btn-default",
                click: function () {
                    $(this).dialog("close");

                }
            }, {

                html: "<i class='fa fa-plus'></i>&nbsp; Add",
                "class": "btn sa-btn-danger",
                click: function () {
                    addTab();
                    $(this).dialog("close");
                }
            }]
        });
        // addTab form: calls addTab function on submit and closes the dialog
        var form = dialog.find("form").submit(function (event) {
            addTab();
            dialog.dialog("close");
            event.preventDefault();
        });

        // actual addTab function: adds new tab using the input from the form above

    </script>

    <script>
        $(document).on("change", ".status", function () {
            var status = $(this).val();
            var id = $(this).attr("uid");
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('admin.settings.edit.status') }}",
                type: "post",
                dataType: "json",
                data: {status: status, id: id, _token: token},
                success: function (data) {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: data.message,
                        showConfirmButton: false,
                        width: '40rem',
                        customClass: "right-check",
                        timer: 1500
                    });
                    if (data.status !== "1") {
                        Swal.fire({
                            type: 'error',
                            title: 'حدث خطأ',
                        });
                    }
                },
                error: function () {
                    Swal.fire({
                        type: 'error',
                        title: 'حدث خطأ',
                    });
                }
            })
        })
    </script>

@stop