@extends('admin.layouts.Master-Layout')

@section('title')

    Mobile

@stop

@section('content')

    <!-- BEGIN .sa-content-wrapper -->
    <div class="sa-content-wrapper">
        <!-- BEGIN .sa-page-breadcrumb -->
        <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
            <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets" style="background: #1b1e24;"><i
                            class="fa fa-refresh"></i></span></li>
            <li class="breadcrumb-item"><a href="flot.html" style="color:black;">Supervisor</a></li>
            <li class="breadcrumb-item-1"><a href="datatables.html"></a></li>
        </ol>
        <!-- END .sa-page-breadcrumb -->
        <div class="sa-content">
            <div class="d-flex w-100 home-header">
                <div>
                    <h1 class="page-header"><i class="fa fa-table fa-fw "></i> Dashboard <span>> Supervisor
								</span></h1>
                </div>
            </div>
            <div>
                <div>
                    <!-- widget grid -->
                    <section id="widget-grid" class="">
                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="well well-sm well-light">

                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#main" role="tab">Main Category </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#manfucatures" role="tab"> Manfucatures</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#modules" role="tab">Modules </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#memory" role="tab">Memory</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#network" role="tab">Networks</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#condition" role="tab">Conditions</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#store" role="tab">Stores</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#sproduct-sell" role="tab">Sell Products</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#bproduct-buy" role="tab">Buy Products</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#osell" role="tab">Orders Sell</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#obuy" role="tab">Order Buy</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane" id="main" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-main"> Add Main Category</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-main" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    User</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.mobile.add.category')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-cata" accept=".gif, .jpg, .png" />
                                                                            <label for="image-cata" class="file-ok">
                                                                                <span>{{trans('admin.image')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($categories as $category)

                                                        <tr>

                                                            <td>{{$category->id}}</td>
                                                            <td>{{$category->name_ar}}</td>
                                                            <td>{{$category->name_en}}</td>


                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/category/' . $category->img)}}" data-img="{{asset('uploads/category/' . $category->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-cated{{$category->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-cate{{$category->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>


                                                        @endforeach

                                                        @foreach($categories as $category)


                                                            <!-- Modal  to Edit main category-->
                                                        <div class="modal fade" id="myModal-cated{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.mobile.edit.category',['id'=>$category->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$category->name_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$category->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image-cate{{$category->id}}" accept=".gif, .jpg, .png" />
                                                                                    <label for="image-cate{{$category->id}}"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/category/'.$category->img)}});">
                                                                                        <span>{{trans('admin.image')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-catd{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.mobile.delete.category',['id'=>$category->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            {{trans('admin.do you want to')}}
                                                                            <strong> {{trans('admin.delete this member')}} </strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                            <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="manfucatures" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-manf"> Add Manfucatures</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-manf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    User</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.mobile.add.manufacture')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="category_id" class="form-control">
                                                                                @foreach($categories as $category)
                                                                                    <option value="{{$category->id}}">{{$category->name_ar}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-manu" accept=".gif, .jpg, .png" />
                                                                            <label for="image-manu" class="file-ok">
                                                                                <span>{{trans('admin.image')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-hide="phone">{{trans('admin.arabic image department')}}</th>
                                                            <th data-hide="phone">{{trans('admin.english image department')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($manufactures as $manufacture)

                                                        <tr>

                                                            <td>{{$manufacture->id}}</td>
                                                            <td>{{$manufacture->name_ar}}</td>
                                                            <td>{{$manufacture->name_en }}</td>

                                                            <td>
                                                                {{$manufacture->categories['name_ar']}}
                                                            </td>
                                                            <td>
                                                                {{$manufacture->categories['name_en']}}
                                                            </td>

                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/manufacture/' . $manufacture->img)}}" data-img="{{asset('uploads/manufacture/' . $manufacture->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-manued{{$manufacture->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-manud{{$manufacture->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        @endforeach

                                                        @foreach($manufactures as $manufacture)



                                                        <!-- Modal  to Edit manf-->
                                                        <div class="modal fade" id="myModal-manued{{$manufacture->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.mobile.edit.manufacture',['id'=>$manufacture->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$manufacture->name_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$manufacture->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                                </div>
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="category_id" class="form-control">
                                                                                        <option selected disabled>{{app()->isLocale('ar') ? $manufacture->categories->name_ar: $manufacture->categories->name_en}}</option>
                                                                                        @foreach($categories as $category)
                                                                                            <option value="{{$category->id}}">{{app()->isLocale('ar') ? $category->name_ar: $category->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image6-manuf{{$manufacture->id}}" accept=".gif, .jpg, .png" />
                                                                                    <label for="image6-manuf{{$manufacture->id}}"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/manufacture/'.$manufacture->img)}});" >
                                                                                        <span>{{trans('admin.upload img')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-manud{{$manufacture->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.mobile.delete.manufacture',['id'=>$manufacture->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            {{trans('admin.do you want to')}}
                                                                            <strong>{{trans('admin.delete this image')}}</strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                            <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                            @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="modules" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-modul"> Add Modules</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-modul" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    User</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.mobile.add.module')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="manufacture_id" class="form-control">
                                                                                @foreach($manufactures as $manufacture)
                                                                                    <option value="{{$manufacture->id}}">{{$manufacture->name_ar}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-module" accept=".gif, .jpg, .png" />
                                                                            <label for="image-module" class="file-ok">
                                                                                <span>{{trans('admin.image')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-hide="phone">{{trans('admin.arabic image department')}}</th>
                                                            <th data-hide="phone">{{trans('admin.english image department')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($modules as $module)

                                                        <tr>

                                                            <td>{{$module->id}}</td>
                                                            <td>{{$module->name_ar}}</td>
                                                            <td>{{$module->name_en }}</td>

                                                            <td>
                                                                {{$module->manufactures['name_ar']}}
                                                            </td>
                                                            <td>
                                                                {{$module->manufactures['name_en']}}
                                                            </td>


                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/module/' . $module->img)}}" data-img="{{asset('uploads/module/' . $module->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModaled-module{{$module->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModalde-module{{$module->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        @endforeach

                                                        @foreach($modules as $module)

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModaled-module{{$module->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.mobile.edit.module',['id'=>$module->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$manufacture->name_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$manufacture->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="manufacture_id" class="form-control">
                                                                                        <option selected disabled>{{app()->isLocale('ar') ? $module->manufactures->name_ar: $module->manufactures->name_en}}</option>
                                                                                        @foreach($manufactures as $manufacture)
                                                                                            <option value="{{$manufacture->id}}">{{app()->isLocale('ar') ? $manufacture->name_ar: $manufacture->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image6-g" accept=".gif, .jpg, .png" />
                                                                                    <label for="image6-g"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/module/'.$module->img)}});" >
                                                                                        <span>{{trans('admin.upload img')}}</span>
                                                                                    </label>
                                                                                </div>


                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModalde-module{{$module->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.mobile.delete.module',['id'=>$module->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            {{trans('admin.do you want to')}}
                                                                            <strong>{{trans('admin.delete this image')}}</strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                            <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="memory" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-memory"> Add Memory</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-memory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    User</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.mobile.add.memory')}}"
                                                                      method="post">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Name In Arabic</label>
                                                                            <input type="number" name="size" class="form-control" id="exampleInputText1" placeholder="Size">
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-hide="phone"> Size</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($memories as $memory)

                                                        <tr>

                                                            <td>{{$memory->id}}</td>
                                                            <td>{{$memory->size}}</td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModaled-memory{{$memory->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModalde-memory{{$memory->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        @endforeach
                                                        @foreach($memories as $memory)


                                                            <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModaled-memory{{$memory->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.mobile.edit.memory',['id'=>$memory->id])}}"
                                                                              method="post">
                                                                            @csrf
                                                                            <div class="row">

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Name In Arabic</label>
                                                                                    <input type="number" name="size" value="{{$memory->size}}" class="form-control" id="exampleInputText1">
                                                                                </div>

                                                                            </div>

                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModalde-memory{{$memory->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.mobile.delete.memory',['id'=>$memory->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            {{trans('admin.do you want to')}}
                                                                            <strong>{{trans('admin.delete this image')}}</strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                            <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="network" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-net"> Add Network</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-net" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    User</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.mobile.add.network')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-network" accept=".gif, .jpg, .png" />
                                                                            <label for="image-network" class="file-ok">
                                                                                <span>{{trans('admin.image')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>

                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($networks as $network)

                                                        <tr>

                                                            <td>{{$network->id}}</td>
                                                            <td>{{$network->name_ar}}</td>
                                                            <td>{{$network->name_en }}</td>

                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/network/' . $network->img)}}" data-img="{{asset('uploads/network/' . $network->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModaled-net{{$network->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModalde-net{{$network->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        @endforeach

                                                        @foreach($networks as $network)

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModaled-net{{$network->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.mobile.edit.network',['id'=>$network->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$network->name_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$network->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                                </div>
                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image6-net" accept=".gif, .jpg, .png" />
                                                                                    <label for="image6-net"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/network/'.$network->img)}});" >
                                                                                        <span>{{trans('admin.upload img')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>

                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModalde-net{{$network->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.mobile.delete.network',['id'=>$network->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            {{trans('admin.do you want to')}}
                                                                            <strong>{{trans('admin.delete this image')}}</strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                            <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="condition" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-con"> Add Condition</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-con" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    User</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.mobile.add.mobile.condition')}}"
                                                                      method="post">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">
                                                                                Arabic First</label>
                                                                            <textarea class="form-control" name="cond1_ar" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">English First
                                                                            </label>
                                                                            <textarea class="form-control" name="cond1_en" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">Arabic Second
                                                                            </label>
                                                                            <textarea class="form-control" name="cond2_ar" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">English Second
                                                                            </label>
                                                                            <textarea class="form-control" name="cond2_en" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">
                                                                                Arabic third</label>
                                                                            <textarea class="form-control" name="cond3_ar" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">English third
                                                                            </label>
                                                                            <textarea class="form-control" name="cond3_en" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">Arabic fourth
                                                                            </label>
                                                                            <textarea class="form-control" name="cond4_ar" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">English Fourth
                                                                            </label>
                                                                            <textarea class="form-control" name="cond4_en" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3"></textarea>
                                                                        </div>
                                                                    </div>

                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover table-responsive" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>

                                                            <th data-hide="phone"> Arabic 1</th>
                                                            <th data-hide="phone"> English 1</th>
                                                            <th data-hide="phone"> Arabic 2</th>
                                                            <th data-hide="phone"> English 2</th>
                                                            <th data-hide="phone"> Arabic 3</th>
                                                            <th data-hide="phone"> English 3</th>
                                                            <th data-hide="phone"> Arabic 4</th>
                                                            <th data-hide="phone"> English 4</th>

                                                            <th data-hide="phone">Image</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($mobile_conditions as $mobile_condition)

                                                        <tr>

                                                            <td>{{$mobile_condition->id}}</td>
                                                            <td>{{$mobile_condition->name_ar}}</td>
                                                            <td>{{$mobile_condition->name_en}}</td>

                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-cond1{{$mobile_condition->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-cond2{{$mobile_condition->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-cond3{{$mobile_condition->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-cond4{{$mobile_condition->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-cond5{{$mobile_condition->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-cond6{{$mobile_condition->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-cond7{{$mobile_condition->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-cond8{{$mobile_condition->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>

                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModaled-cond{{$mobile_condition->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModalde-cond{{$mobile_condition->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>


                                                        @endforeach

                                                        @foreach($mobile_conditions as $mobile_condition)

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModaled-cond{{$mobile_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.mobile.edit.mobile.condition',['id'=>$mobile_condition->id])}}"
                                                                              method="post">
                                                                            @csrf
                                                                            <div class="row">

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$mobile_condition->name_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$mobile_condition->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">
                                                                                        Arabic First</label>
                                                                                    <textarea class="form-control" name="cond1_ar" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3">{{$mobile_condition->cond1_ar}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">English first
                                                                                    </label>
                                                                                    <textarea class="form-control" name="cond1_en" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3">{{$mobile_condition->cond1_en}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">Arabic second
                                                                                    </label>
                                                                                    <textarea class="form-control" name="cond2_ar" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3">{{$mobile_condition->cond2_ar}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">English Second
                                                                                    </label>
                                                                                    <textarea class="form-control" name="cond2_en" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3">{{$mobile_condition->cond2_en}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">
                                                                                        Arabic third</label>
                                                                                    <textarea class="form-control" name="cond3_ar" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3">{{$mobile_condition->cond3_ar}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">English Third
                                                                                    </label>
                                                                                    <textarea class="form-control" name="cond3_en" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3">{{$mobile_condition->cond3_en}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">Arabic Fourth
                                                                                    </label>
                                                                                    <textarea class="form-control" name="cond4_ar" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3">{{$mobile_condition->cond4_ar}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">English Fourth
                                                                                    </label>
                                                                                    <textarea class="form-control" name="cond4_en" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3">{{$mobile_condition->cond4_en}}</textarea>
                                                                                </div>

                                                                            </div>

                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModalde-cond{{$mobile_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.mobile.delete.mobile.condition',['id'=>$mobile_condition->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            {{trans('admin.do you want to')}}
                                                                            <strong>{{trans('admin.delete this image')}}</strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                            <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-cond1{{$mobile_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic cond desc')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$mobile_condition->cond1_ar}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-cond2{{$mobile_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic cond desc')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$mobile_condition->cond1_en}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-cond3{{$mobile_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic cond desc')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$mobile_condition->cond2_ar}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-cond4{{$mobile_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic cond desc')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$mobile_condition->cond2_en}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-cond5{{$mobile_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic cond desc')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$mobile_condition->cond3_ar}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-cond6{{$mobile_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic cond desc')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$mobile_condition->cond3_en}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-cond7{{$mobile_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic cond desc')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$mobile_condition->cond4_ar}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-cond8{{$mobile_condition->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            {{trans('admin.arabic cond desc')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$mobile_condition->cond4_en}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="store" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-store"> Add Image</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-store" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    User</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.mobile.add.store')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">Arabic Address
                                                                            </label>
                                                                            <textarea name="address_ar" class="form-control" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleFormControlTextarea1">English Adress
                                                                            </label>
                                                                            <textarea name="address_en" class="form-control" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3"></textarea>
                                                                        </div>



                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-store" accept=".gif, .jpg, .png" />
                                                                            <label for="image-store" class="file-ok">
                                                                                <span>{{trans('admin.image')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">{{trans('admin.id')}}</th>
                                                            <th data-class="expand">{{trans('admin.arabic name')}}</th>
                                                            <th data-class="expand">{{trans('admin.english name')}}</th>
                                                            <th data-hide="phone">{{trans('admin.arabic image department')}}</th>
                                                            <th data-hide="phone">{{trans('admin.english image department')}}</th>
                                                            <th data-hide="phone">{{trans('admin.image')}}</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($stores as $store)

                                                        <tr>

                                                            <td>{{$store->id}}</td>
                                                            <td>{{$store->name_ar}}</td>
                                                            <td>{{$store->name_en }}</td>

                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-addrar{{$store->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-addren{{$store->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>


                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/store/' . $store->img)}}" data-img="{{asset('uploads/store/' . $store->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModaled-store{{$store->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModalde-store{{$store->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        @endforeach

                                                        @foreach($stores as $store)
                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-addrar{{$store->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Image Text</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$store->address_ar}}</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-addren{{$store->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Image Text</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$store->address_en}}</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModaled-store{{$store->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.mobile.edit.store',['id'=>$store->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.arabic name')}}</label>
                                                                                    <input type="text" name="name_ar" value="{{$store->name_ar}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.arabic name')}}">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">{{trans('admin.english name')}}</label>
                                                                                    <input type="text" name="name_en" value="{{$store->name_en}}" class="form-control" id="exampleInputText1" placeholder="{{trans('admin.english name')}}">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">Arabic Address
                                                                                    </label>
                                                                                    <textarea name="address_ar" class="form-control" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3">{{$store->address_ar}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">English Adress
                                                                                    </label>
                                                                                    <textarea name="address_en" class="form-control" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3">{{$store->address_en}}</textarea>
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image6-store" accept=".gif, .jpg, .png" />
                                                                                    <label for="image6-store"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/module/'.$module->img)}});" >
                                                                                        <span>{{trans('admin.upload img')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>

                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModalde-store{{$store->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.mobile.delete.store',['id'=>$store->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            {{trans('admin.do you want to')}}
                                                                            <strong>{{trans('admin.delete this image')}}</strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                            <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="sproduct-sell" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-spr"> Add Sell Product</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-spr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    User</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.mobile.add.sell.product')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="category_id" class="form-control">
                                                                                @foreach($categories as $category)
                                                                                    <option value="{{$category->id}}">{{$category->name_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="manufacture_id" class="form-control">
                                                                                @foreach($manufactures as $manufacture)
                                                                                    <option value="{{$manufacture->id}}">{{$manufacture->name_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="module_id" class="form-control">
                                                                                @foreach($modules as $module)
                                                                                    <option value="{{$module->id}}">{{$module->name_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="memory_id" class="form-control">
                                                                                @foreach($memories as $memory)
                                                                                    <option value="{{$memory->id}}">{{$memory->size}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="network_id" class="form-control">
                                                                                @foreach($networks as $network)
                                                                                    <option value="{{$network->id}}">{{$network->name_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="condition_id" class="form-control">
                                                                                @foreach($mobile_conditions as $mobile_condition)
                                                                                    <option value="{{$mobile_condition->id}}">{{$mobile_condition->name_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-sell" accept=".gif, .jpg, .png" />
                                                                            <label for="image-sell" class="file-ok">
                                                                                <span>{{trans('admin.image')}}</span>
                                                                            </label>
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">  Value</label>
                                                                            <input type="number" name="value" class="form-control" id="exampleInputText1" placeholder="Value">
                                                                        </div>
                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">  Main Category</th>
                                                            <th data-class="expand">  Manfacures</th>
                                                            <th data-hide="phone">Modules</th>
                                                            <th data-hide="phone">Memory</th>
                                                            <th data-hide="phone">Network</th>
                                                            <th data-hide="phone">Condition</th>
                                                            <th data-hide="phone">Value</th>
                                                            <th data-hide="phone">Image</th>

                                                            <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($sell_products as $sell_product)


                                                            <tr>

                                                                <td>{{$sell_product->id}}</td>
                                                                <td>{{$sell_product->categories->name_en}}</td>
                                                                <td>{{$sell_product->manufactures->name_en}}</td>
                                                                <td>{{$sell_product->modules->name_en}}</td>
                                                                <td>{{$sell_product->memories->size}}</td>
                                                                <td>{{$sell_product->networks->name_en}}</td>
                                                                <td>{{$sell_product->conditions->name_en}}</td>
                                                                <td>{{$sell_product->value}}</td>

                                                                <td>
                                                                    <div class="superbox-list superbox-8">
                                                                        <img src="{{asset('uploads/sell-product/' . $sell_product->img)}}" data-img="{{asset('uploads/sell-product/' . $sell_product->img)}}"
                                                                             alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                    </div>
                                                                </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModaled-sell{{$sell_product->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModalde-sell{{$sell_product->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        @endforeach

                                                        @foreach($sell_products as $sell_product)

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModaled-sell{{$sell_product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.mobile.edit.sell.product',['id'=>$sell_product->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="category_id" class="form-control">
                                                                                        <option selected disabled>{{$sell_product->categories->name_en}}</option>
                                                                                        @foreach($categories as $category)
                                                                                            <option value="{{$category->id}}">{{$category->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="manufacture_id" class="form-control">
                                                                                        <option selected disabled>{{$sell_product->manufactures->name_en}}</option>
                                                                                        @foreach($manufactures as $manufacture)
                                                                                            <option value="{{$manufacture->id}}">{{$manufacture->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="module_id" class="form-control">
                                                                                        <option selected disabled>{{$sell_product->modules->name_en}}</option>
                                                                                        @foreach($modules as $module)
                                                                                            <option value="{{$module->id}}">{{$module->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="memory_id" class="form-control">
                                                                                        <option selected disabled>{{$sell_product->memories->name_en}}</option>
                                                                                        @foreach($memories as $memory)
                                                                                            <option value="{{$memory->id}}">{{$memory->size}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="network_id" class="form-control">
                                                                                        <option selected disabled>{{$sell_product->networks->name_en}}</option>
                                                                                        @foreach($networks as $network)
                                                                                            <option value="{{$network->id}}">{{$network->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="condition_id" class="form-control">
                                                                                        <option selected disabled>{{$sell_product->conditions->name_en}}</option>
                                                                                        @foreach($mobile_conditions as $mobile_condition)
                                                                                            <option value="{{$mobile_condition->id}}">{{$mobile_condition->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">  Value</label>
                                                                                    <input type="number" name="value" value="{{$sell_product->value}}" class="form-control" id="exampleInputText1" placeholder="Value">
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image6-sell" accept=".gif, .jpg, .png" />
                                                                                    <label for="image6-sell"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/sell-product/'.$mobile_condition->img)}});" >
                                                                                        <span>{{trans('admin.upload img')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModalde-sell{{$sell_product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.mobile.delete.sell.product',['id'=>$sell_product->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            {{trans('admin.do you want to')}}
                                                                            <strong>{{trans('admin.delete this image')}}</strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                            <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="bproduct-buy" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-bpr"> Add Buy Product</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-bpr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    User</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.mobile.add.buy.product')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="category_id" class="form-control">
                                                                                @foreach($categories as $category)
                                                                                    <option value="{{$category->id}}">{{$category->name_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="manufacture_id" class="form-control">
                                                                                @foreach($manufactures as $manufacture)
                                                                                    <option value="{{$manufacture->id}}">{{$manufacture->name_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="module_id" class="form-control">
                                                                                @foreach($modules as $module)
                                                                                    <option value="{{$module->id}}">{{$module->name_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="memory_id" class="form-control">
                                                                                @foreach($memories as $memory)
                                                                                    <option value="{{$memory->id}}">{{$memory->size}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="network_id" class="form-control">
                                                                                @foreach($networks as $network)
                                                                                    <option value="{{$network->id}}">{{$network->name_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                            <select name="condition_id" class="form-control">
                                                                                @foreach($mobile_conditions as $mobile_condition)
                                                                                    <option value="{{$mobile_condition->id}}">{{$mobile_condition->name_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-buy" accept=".gif, .jpg, .png" />
                                                                            <label for="image-buy" class="file-ok">
                                                                                <span>{{trans('admin.image')}}</span>
                                                                            </label>
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">  Value</label>
                                                                            <input type="number" name="value" class="form-control" id="exampleInputText1" placeholder="Value">
                                                                        </div>
                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">  Main Category</th>
                                                            <th data-class="expand">  Manfacures</th>
                                                            <th data-hide="phone">Modules</th>
                                                            <th data-hide="phone">Memory</th>
                                                            <th data-hide="phone">Network</th>
                                                            <th data-hide="phone">Condition</th>
                                                            <th data-hide="phone">Value</th>
                                                            <th data-hide="phone">Image</th>

                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($buy_products as $buy_product)

                                                        <tr>

                                                            <td>{{$buy_product->id}}</td>
                                                            <td>{{$buy_product->categoriesb->name_en}}</td>
                                                            <td>{{$buy_product->manufacturesb->name_en}}</td>
                                                            <td>{{$buy_product->modulesb->name_en}}</td>
                                                            <td>{{$buy_product->memoriesb->size}}</td>
                                                            <td>{{$buy_product->networksb->name_en}}</td>
                                                            <td>{{$buy_product->conditionsb->name_en}}</td>
                                                            <td>{{$buy_product->value}}</td>

                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/buy-product/' . $buy_product->img)}}" data-img="{{asset('uploads/buy-product/' . $buy_product->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModaled-buy{{$buy_product->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModalde-buy{{$buy_product->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        @endforeach

                                                        @foreach($buy_products as $buy_product)

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModaled-buy{{$buy_product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.mobile.edit.buy.product',['id'=>$buy_product->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="category_id" class="form-control">
                                                                                        <option selected disabled>{{$buy_product->categoriesb->name_en}}</option>
                                                                                        @foreach($categories as $category)
                                                                                            <option value="{{$category->id}}">{{$category->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="manufacture_id" class="form-control">
                                                                                        <option selected disabled>{{$buy_product->manufacturesb->name_en}}</option>
                                                                                        @foreach($manufactures as $manufacture)
                                                                                            <option value="{{$manufacture->id}}">{{$manufacture->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="module_id" class="form-control">
                                                                                        <option selected disabled>{{$buy_product->modulesb->name_en}}</option>
                                                                                        @foreach($modules as $module)
                                                                                            <option value="{{$module->id}}">{{$module->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="memory_id" class="form-control">
                                                                                        <option selected disabled>{{$buy_product->memoriesb->name_en}}</option>
                                                                                        @foreach($memories as $memory)
                                                                                            <option value="{{$memory->id}}">{{$memory->size}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="network_id" class="form-control">
                                                                                        <option selected disabled>{{$buy_product->networksb->name_en}}</option>
                                                                                        @foreach($networks as $network)
                                                                                            <option value="{{$network->id}}">{{$network->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">{{trans('admin.arabic image department')}} </label>
                                                                                    <select name="condition_id" class="form-control">
                                                                                        <option selected disabled>{{$buy_product->conditionsb->name_en}}</option>
                                                                                        @foreach($mobile_conditions as $mobile_condition)
                                                                                            <option value="{{$mobile_condition->id}}">{{$mobile_condition->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">  Value</label>
                                                                                    <input type="number" name="value" value="{{$buy_product->value}}" class="form-control" id="exampleInputText1" placeholder="Value">
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image6-buy" accept=".gif, .jpg, .png" />
                                                                                    <label for="image6-buy"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/buy-product/'.$mobile_condition->img)}});" >
                                                                                        <span>{{trans('admin.upload img')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save changes')}}</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModalde-buy{{$buy_product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.mobile.delete.buy.product',['id'=>$buy_product->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            {{trans('admin.do you want to')}}
                                                                            <strong>{{trans('admin.delete this image')}}</strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('admin.no')}}</button>
                                                                            <button type="submit" class="btn btn-primary">{{trans('admin.yes')}}</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach


                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="osell" role="tabpanel">
                                            <div style="text-align: center;">

                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">Main</th>
                                                            <th data-class="expand">Manfacure</th>
                                                            <th data-hide="phone">Modules</th>
                                                            <th data-hide="phone">Memory</th>
                                                            <th data-hide="phone">Network</th>
                                                            <th data-hide="phone">Condition</th>
                                                            <th data-hide="phone">Store</th>
                                                            <th data-hide="phone">Value</th>
                                                            <th data-hide="phone">Image</th>
                                                            <th data-hide="phone">User Details</th>
                                                            <th data-hide="phone">Done</th>
                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($orders_sell as $order_sell)

                                                        <tr>

                                                            <td>{{$order_sell->id}}</td>
                                                            <td>{{$order_sell->category_name}}</td>
                                                            <td>{{$order_sell->manufacture_name}}</td>
                                                            <td>{{$order_sell->model_name}}</td>
                                                            <td>{{$order_sell->memory}}</td>
                                                            <td>{{$order_sell->network}}</td>
                                                            <td>{{$order_sell->condition}}</td>
                                                            <td>{{$order_sell->store}}</td>
                                                            <td>{{$order_sell->value}}</td>



                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/sell-product/' . $order_sell->img)}}" data-img="{{asset('uploads/sell-product/' . $order_sell->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-detail-ar{{$order_sell->id}}" style="margin-top:10px;color:#fff">Show</button>
                                                            </td>

                                                            <td>
                                                                <div class="form-group">
                                                                    <select class="form-control status"
                                                                            uid="{{ $order_sell->id }}">
                                                                        <option @if($order_sell->is_contacted) selected
                                                                                @endif value="1">
                                                                            Yes
                                                                        </option>
                                                                        <option @if(!$order_sell->is_contacted) selected
                                                                                @endif value="0">
                                                                            No
                                                                        </option>
                                                                    </select>

                                                                </div>
                                                            </td>


                                                            <td>

                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-orderd{{$order_sell->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                        @endforeach

                                                        @foreach($orders_sell as $order_sell)


                                                            <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-detail-ar{{$order_sell->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Image Text</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> Name:</p> {{$order_sell->name}}
                                                                        <p> email:</p> {{$order_sell->email}}
                                                                        <p> phone:</p> {{$order_sell->phone}}

                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-orderd{{$order_sell->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.mobile.delete.sell',['id'=>$order_sell->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        Are you sure you want to
                                                                        <strong>Delet This
                                                                            Image </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="obuy" role="tabpanel">
                                            <div style="text-align: center;">

                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">Main</th>
                                                            <th data-class="expand">Manfacure</th>
                                                            <th data-hide="phone">Modules</th>
                                                            <th data-hide="phone">Memory</th>
                                                            <th data-hide="phone">Network</th>
                                                            <th data-hide="phone">Condition</th>
                                                            <th data-hide="phone">Store</th>
                                                            <th data-hide="phone">Value</th>
                                                            <th data-hide="phone">Image</th>
                                                            <th data-hide="phone">User Details</th>
                                                            <th data-hide="phone">Done</th>
                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($orders_buy as $order_buy)

                                                            <tr>

                                                                <td>{{$order_buy->id}}</td>
                                                                <td>{{$order_buy->category_name}}</td>
                                                                <td>{{$order_buy->manufacture_name}}</td>
                                                                <td>{{$order_buy->model_name}}</td>
                                                                <td>{{$order_buy->memory}}</td>
                                                                <td>{{$order_buy->network}}</td>
                                                                <td>{{$order_buy->condition}}</td>
                                                                <td>{{$order_buy->store}}</td>
                                                                <td>{{$order_buy->value}}</td>



                                                                <td>
                                                                    <div class="superbox-list superbox-8">
                                                                        <img src="{{asset('uploads/buy-product/' . $order_buy->img)}}" data-img="{{asset('uploads/buy-product/' . $order_buy->img)}}"
                                                                             alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                    </div>
                                                                </td>

                                                                <td>
                                                                    <button class="btn btn-info" data-toggle="modal" data-target="#myModal-detailb-ar{{$order_buy->id}}" style="margin-top:10px;color:#fff">Show</button>
                                                                </td>

                                                                <td>
                                                                    <div class="form-group">
                                                                        <select class="form-control status-buy"
                                                                                bid="{{ $order_buy->id }}">
                                                                            <option @if($order_buy->is_contacted) selected
                                                                                    @endif value="1">
                                                                                Yes
                                                                            </option>
                                                                            <option @if(!$order_buy->is_contacted) selected
                                                                                    @endif value="0">
                                                                                No
                                                                            </option>
                                                                        </select>

                                                                    </div>
                                                                </td>


                                                                <td>

                                                                    <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-orderdb{{$order_buy->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-trash"></i></button>

                                                                </td>

                                                            </tr>
                                                        @endforeach

                                                        @foreach($orders_buy as $order_buy)


                                                            <!-- Modal  to show image-->
                                                            <div class="modal fade" id="myModal-detailb-ar{{$order_buy->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                                Image Text</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <p> Name:</p> {{$order_buy->name}}
                                                                            <p> email:</p> {{$order_buy->email}}
                                                                            <p> phone:</p> {{$order_buy->phone}}

                                                                        </div>
                                                                        <div class="modal-footer" style="margin-top:1rem;">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <!-- Modal to delet image -->
                                                            <div class="modal fade" id="myModal-orderdb{{$order_buy->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                                Image</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <form action="{{route('admin.mobile.delete.buy',['id'=>$order_buy->id])}}"
                                                                              method="post">
                                                                            @csrf
                                                                            <div class="modal-body" style="text-align:left;">
                                                                                Are you sure you want to
                                                                                <strong>Delet This
                                                                                    Image </strong> ?
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                                <button type="submit" class="btn btn-primary">Yes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- end row -->

                        <!-- end row -->

                    </section>
                    <!-- end widget grid -->

                </div>
            </div>
        </div>

@stop


@section('scripts')


            <script>
                $(document).on("change", ".status", function () {
                    var status = $(this).val();
                    var id = $(this).attr("uid");
                    var token = "{{ csrf_token() }}";
                    $.ajax({
                        url: "{{ route('admin.mobile.edit.sell') }}",
                        type: "post",
                        dataType: "json",
                        data: {status: status, id: id, _token: token},
                        success: function (data) {
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: data.message,
                                showConfirmButton: false,
                                width: '40rem',
                                customClass: "right-check",
                                timer: 1500
                            });
                            if (data.status !== "1") {
                                Swal.fire({
                                    type: 'error',
                                    title: 'حدث خطأ',
                                });
                            }
                        },
                        error: function () {
                            Swal.fire({
                                type: 'error',
                                title: 'حدث خطأ',
                            });
                        }
                    })
                })
            </script>

            <script>
                $(document).on("change", ".status-buy", function () {
                    var status = $(this).val();
                    var id = $(this).attr("bid");
                    var token = "{{ csrf_token() }}";
                    $.ajax({
                        url: "{{ route('admin.mobile.edit.buy') }}",
                        type: "post",
                        dataType: "json",
                        data: {status: status, id: id, _token: token},
                        success: function (data) {
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: data.message,
                                showConfirmButton: false,
                                width: '40rem',
                                customClass: "right-check",
                                timer: 1500
                            });
                            if (data.status !== "1") {
                                Swal.fire({
                                    type: 'error',
                                    title: 'حدث خطأ',
                                });
                            }
                        },
                        error: function () {
                            Swal.fire({
                                type: 'error',
                                title: 'حدث خطأ',
                            });
                        }
                    })
                })
            </script>


    @stop