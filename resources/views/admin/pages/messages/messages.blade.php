@extends('admin.layouts.Master-Layout')

@section('title')

    {{trans('admin.messages')}}

@stop

@section('content')

    <div class="sa-content-wrapper">

        <div class="sa-content">
            <div class="d-flex w-100 home-header">
                <div>
                    <h1 class="page-header"><i class="fa fa-table fa-fw "></i> {{trans('admin.dashboard')}} <span>> {{trans('admin.messages')}}
								</span></h1>
                </div>
            </div>
            <div>
                <div>
                    <!-- widget grid -->
                    <section id="widget-grid" class="">
                        <!-- row -->
                        <div class="row">
                            <!-- NEW WIDGET START -->
                            <article class="col-12">
                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-3"
                                     data-widget-editbutton="false">
                                    <header>
                                        <div class="widget-header">
													<span class="widget-icon"> <i
                                                                class="fa fa-lg fa-fw fa-envelope-square"></i> </span>
                                        </div>
                                    </header>
                                    <!-- widget div-->
                                    <div style="text-align: center;">
                                        <a style=" margin-top: 1rem;" href="javascript:void(0);"
                                           class="btn sa-btn-danger" data-toggle="modal"
                                           data-target="#myModal-1"> {{trans('admin.delete.all')}}</a>

                                        <!-- Modal  to add delet-->
                                        <div class="modal fade" id="myModal-1" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"
                                                            style="font-weight:bold">{{trans('admin.delete.all')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align:left;">
                                                       {{trans('admin.delete')}}
                                                    </div>
                                                    <form action="{{route('admin.messages.delete.all')}}"
                                                          method="post">
                                                        @csrf
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">{{trans('admin.cancel')}}
                                                            </button>
                                                            <button type="submit"
                                                                    class="btn btn-primary">{{trans('admin.confirm')}}
                                                            </button>
                                                        </div>
                                                    </form>

                                                </div>

                                            </div>
                                        </div>
                                        <!-- widget content -->
                                        <div class="widget-body p-0">


                                            <table id="dt-basic"
                                                   class="table table-striped table-bordered table-hover"
                                                   width="100%">
                                                <thead>
                                                <tr>
                                                    <th data-hide="phone">{{trans('admin.id')}}</th>
                                                    <th data-class="expand">{{trans('admin.name')}}</th>

                                                    <th data-hide="phone">{{trans('admin.email')}}</th>
                                                    <th data-hide="phone">{{trans('admin.phone')}}</th>
                                                    <th data-hide="phone">{{trans('admin.title')}}</th>
                                                    <th data-hide="phone">{{trans('admin.content')}}</th>


                                                    <th data-hide="phone,tablet">{{trans('admin.active')}}</th>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($messages as $message)
                                                    <tr>
                                                        <td>{{$message->id}}</td>
                                                        <td>{{$message->name}}</td>
                                                        <td>{{$message->email}}</td>
                                                        <td>{{$message->phone}}</td>
                                                        <td>{{$message->title}}</td>


                                                        <td>
                                                            <button class="btn btn-info" data-toggle="modal"
                                                                    data-target="#myModal-4{{$message->id}}"
                                                                    style="margin-top:10px;color:#fff">{{trans('admin.show')}}
                                                            </button>
                                                        </td>

                                                        <td>
                                                            <button class="btn btn-danger" data-toggle="modal"
                                                                    data-target="#myModal-2{{$message->id}}"
                                                                    style="margin-bottom:10px;"><i
                                                                        class="fa fa-trash"></i></button>


                                                        </td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->

                            </article>
                            <!-- WIDGET END -->

                        </div>

                        <!-- end row -->

                        <!-- end row -->

                    </section>
                    <!-- end widget grid -->

                </div>
            </div>
        </div>
    </div>
    @foreach($messages as $message)

        <!-- Modal to message content -->
        <div class="modal fade" id="myModal-4{{$message->id}}" tabindex="-1"
             role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="exampleModalLabel"
                            style="font-weight:bold">{{trans('admin.content')}}</h5>
                        <button type="button" class="close"
                                data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"
                         style="text-align:left;">
                        <p> {{$message->message}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-default"
                                data-dismiss="modal">{{trans('admin.cancel')}}
                        </button>

                    </div>
                </div>
            </div>
        </div>
        <!-- Modal to delet supervisor -->
        <div class="modal fade" id="myModal-2{{$message->id}}" tabindex="-1"
             role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="exampleModalLabel"
                            style="font-weight:bold">{{trans('admin.delete')}}</h5>
                        <button type="button" class="close"
                                data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"
                         style="text-align:left;">
                       {{trans('admin.delete.confirm')}}
                    </div>
                    <form action="{{route('admin.messages.delete',['id'=>$message->id])}}" method="post">
                        @csrf
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-default"
                                    data-dismiss="modal">{{trans('admin.cancel')}}
                            </button>
                            <button type="submit"
                                    class="btn btn-primary">{{trans('admin.confirm')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@stop