<!DOCTYPE html>
<html lang="en">
<head>

    <title>@yield('title')</title>
    <!--

    Eatery Cafe Template

    http://www.templatemo.com/tm-515-eatery

    -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="{{ asset('css/site.css')}}" media="all" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="{{asset('site/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/templatemo-style.css')}}">


</head>

<body>

@yield('content')




<!-- FOOTER -->
<footer id="footer" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-8">
                <div class="footer-info footer-open-hour text-center">
                    <div class="section-title">
                        <h2 class="wow fadeInUp" data-wow-delay="0.2s">Work Hours</h2>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.4s">
                        <!-- <p>Monday: Closed</p> -->
@foreach(work_days() as $data)
                        <div>
                            <strong>{{$data->day}}</strong>
                            <p>{{$data->from}} {{$data->from_timing}} to {{$data->to}} {{$data->to_timing}}</p>
                        </div>
@endforeach
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-4">
                <ul class="wow fadeInUp social-icon text-center" data-wow-delay="0.4s">
                    <li><a href="{{socials()->facebook}}" class="fa fa-facebook-square" attr="facebook icon"></a></li>
                    <li><a href="{{socials()->twitter}}" class="fa fa-twitter"></a></li>
                    <li><a href="{{socials()->instagram}}" class="fa fa-instagram"></a></li>
                    <li><a href="{{socials()->google_plus}}" class="fa fa-google"></a></li>
                </ul>

                <div class="wow fadeInUp copyright-text text-center" data-wow-delay="0.8s">
                    <p><br>Copyright &copy; 2018 <br>Your Company Name

                        <br><br>Design: <a rel="nofollow" href="http://spectraapps.com/" target="_parent">Spectra</a></p>
                </div>
            </div>

        </div>
    </div>
</footer>

<script type="text/javascript" src="{{asset('js/site.js')}}"></script>

@yield('js')

<script>

    setTimeout(fade_out, 5000);

    function fade_out() {
        $("#checker").fadeOut().empty();
    }

</script>

</body>


<!-- Mirrored from webdesign-finder.com/html/fixetics/index-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Mar 2019 22:21:59 GMT -->
</html>