@extends('layouts.Site-Layout')

@section('title')

    Status

@stop

@section('content')

    <!-- MENU -->
    <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
        <div class="container">

            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                </button>

                <!-- lOGO TEXT HERE -->
                <a href="{{route('site-index')}}" class="navbar-brand">{{$site_details->site_name_en}}</a>
            </div>

            <!-- MENU LINKS -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-nav-first navbar-right" >





                    <li><a href="{{route('site-index')}}" class="smoothScroll">Contact Us</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Start Buying</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Start Selling</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Services</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">About Us</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Home</a></li>
                </ul>

                <!-- <ul class="nav navbar-nav navbar-right">
                     <li><a href="#">Call Now! <i class="fa fa-phone"></i> 010 020 0340</a></li>
                     <a href="#footer" class="section-btn">Reserve a table</a>
                </ul> -->
            </div>

        </div>
    </section>

    <div id="branch">
        <h2 class="text-center" >Choose The Nearest Store</h2>
        <div class="container">
            <div class="row">
                @foreach($stores as $store)
                    <div class="col-lg-4">
                        <div class="br1">

                            <!-- <img src="images/branche.jpg"> -->
                            <a href="#myModal" data-toggle="modal" data-target="#myModal{{$store->id}}">
                                <img src="{{asset('uploads/store/' . $store->img)}}"></a>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div id="add" style="clear:both;">

        @include('errors.errors')

    </div>
    <!-- Modal -->
    @foreach($stores as $store)

        <div class="modal fade" id="myModal{{$store->id}}" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="border:unset;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{$store->name_en}}</h4>
                </div>
                <div class="modal-body" style="border:unset!important;">
                    <form action="{{route('site.sell.mobile')}}#add" method="post" class="wow fadeInUp" id="contact-form"
                          role="form" data-wow-delay="0.8s" dir="rtl" enctype="multipart/form-data">
                        @csrf

                        <div class="col-md-12 col-sm-12" style="padding:3px;">
                            <input type="text" class="form-control" id="cf-name" name="name" placeholder="Name">
                        </div>

                        <div class="col-md-6 col-sm-6" style="padding:3px;">
                            <input type="email" class="form-control" id="cf-email" name="email" placeholder="Email">
                        </div>

                        <div class="col-md-6 col-sm-12" style="padding:3px;">
                            <input type="number" class="form-control" id="cf-phone" name="phone" placeholder="Phone">
                        </div>

                        <div class="col-md-6 col-sm-12" style="padding:3px;">
                            <input type="hidden" class="form-control" id="cf-phone" name="store" value="{{$store->name_en}}">
                        </div>

                        <div class="wrap-custom-file ">
                            <label for="image9{{$store->id}}"  class="file-ok"
                                   style="background-image: url({{url('uploads/buy-product/'. session('img'))}});height:150px;width:150px;">
                            </label>
                        </div>

                        <div class="modal-footer">
                            <button type="submit">Send</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    @endforeach


@stop


@section('js')

@stop