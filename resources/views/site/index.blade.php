@extends('layouts.Site-Layout')

@section('title')

    Mobile

@stop

@section('content')

    <!-- MENU -->
    <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
        <div class="container">

            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                </button>

                <!-- lOGO TEXT HERE -->
                <a href="{{route('site-index')}}" class="navbar-brand">{{$site_details->site_name_en}}</a>
            </div>

            <!-- MENU LINKS -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-nav-first navbar-right" >





                    <li><a href="{{route('site-index')}}" class="smoothScroll">Contact Us</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Start Buying</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Start Selling</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Services</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">About Us</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Home</a></li>
                </ul>

                <!-- <ul class="nav navbar-nav navbar-right">
                     <li><a href="#">Call Now! <i class="fa fa-phone"></i> 010 020 0340</a></li>
                     <a href="#footer" class="section-btn">Reserve a table</a>
                </ul> -->
            </div>

        </div>
    </section>


    <!-- HOME -->
    <section id="home" class="slider" data-stellar-background-ratio="0.5">
        <div class="row">
            @foreach ($web_sliders as $key=>$web_slider)

                @if($key == 0)
            <div class="owl-carousel owl-theme">
                <div class="item item-first" style="background-image: url('{{asset('uploads/slider/' . $web_slider->img)}}') !important;">
                    <div class="caption">
                        <div class="container">
                            <div class="col-md-8 col-sm-12">
                                <!-- <h3>Eatery Cafe &amp; Restaurant</h3>
                                <h1>Our mission is to provide an unforgettable experience</h1>
                                <a href="#team" class="section-btn btn btn-default smoothScroll">Meet our chef</a> -->
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if($key == 1)

                <div class="item item-second" style="background-image: url('{{asset('uploads/slider/' . $web_slider->img)}}') !important;">
                    <div class="caption">
                        <div class="container">
                            <div class="col-md-8 col-sm-12">
                                <!-- <h3>Your Perfect Breakfast</h3>
                                <h1>The best dinning quality can be here too!</h1>
                                <a href="#menu" class="section-btn btn btn-default smoothScroll">Discover menu</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                @endif

                @if($key == 2)

                <div class="item item-third" style="background-image: url('{{asset('uploads/slider/' . $web_slider->img)}}') !important;">
                    <div class="caption">
                        <div class="container">
                            <div class="col-md-8 col-sm-12">
                                <!-- <h3>New Restaurant in Town</h3>
                                <h1>Enjoy our special menus every Sunday and Friday</h1>
                                <a href="#contact" class="section-btn btn btn-default smoothScroll">Reservation</a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                @endif

                @endforeach

        </div>
    </section>


    <!-- ABOUT -->
    <section id="about" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">



                <div class="col-md-6 col-sm-12">
                    <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                        <img src="{{asset('uploads/site_about/' . $about->img1)}}" class="img-responsive" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 text-right">
                    <div class="about-info">
                        <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                            <h4>Read Our Story</h4>
                            <h2>{{$about->title_en}}</h2>
                        </div>

                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                            <p>{{$about->details_en}}</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- TEAM -->

    <section id="services" data-stellar-background-ratio="0.5">
        <h2 class="text-center">Our Services</h2>
        <div class="container">
            <div class="owl-carousel owl-theme ">

                @foreach($services as $service)
                <div class="item">
                    <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">
                        <img src="{{asset('uploads/services/' . $service->img)}}" class="img-responsive" >
                    </div>
                    <div class="team-info">
                        <h3>{{$service->name_en}}</h3>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </section>

    <section id="sell" style="background-image: url('{{asset('site/img/title_banner10.jpg')}}') !important;">

        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center">
                    <h2><a href="{{route('site.buy')}}">Buy Your Device</a></h2>
                </div>
                <div class="col-lg-6 text-center">
                    <h2><a href="{{route('site.sell')}}">Sell Your Device</a></h2>
                </div>
            </div>
        </div>
    </section>


    <!-- CONTACT -->
    <section id="contact" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <!-- How to change your own map point
                        1. Go to Google Maps
                        2. Click on your location point
                        3. Click "Share" and choose "Embed map" tab
                        4. Copy only URL and paste it within the src="" field below
                -->
                <div class="wow fadeInUp col-md-6 col-sm-12" data-wow-delay="0.4s">
                    <div id="google-map">
                        <iframe src="{{$site_details->map}}" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">

                    <div class="col-md-12 col-sm-12">
                        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                            <h2 class="text-right">Contact Us</h2>
                        </div>
                    </div>


                <!-- CONTACT FORM -->
                    <form action="{{route('send.message')}}#message"
                          method="post" class="wow fadeInUp" id="contact-form" role="form" data-wow-delay="0.8s" dir="rtl">
                    @csrf

                        <div class="col-md-6 col-sm-6">
                            <input type="email" class="form-control" id="cf-email" name="email" placeholder="Email">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" id="cf-name" name="name" placeholder="Name">
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <input type="text" class="form-control" id="cf-subject" name="title" placeholder="Title">
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <input type="number" class="form-control" id="cf-phone" name="phone" placeholder="Phone">
                        </div>
                        <div class="col-md-12 col-sm-12">

                            <textarea class="form-control" rows="6" id="cf-message" name="message" placeholder="Message"></textarea>

                            <button type="submit" class="form-control" id="cf-submit" name="submit">Send</button>
                        </div>
                    </form>
                </div>

                <div id="message" style="clear:both;">
                @include('errors.errors')

                </div>

            </div>
        </div>
    </section>

@stop


@section('js')

    @stop