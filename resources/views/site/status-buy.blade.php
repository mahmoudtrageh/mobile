@extends('layouts.Site-Layout')

@section('title')

    Status Buy

@stop

@section('content')

    <!-- MENU -->
    <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
        <div class="container">

            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                </button>

                <!-- lOGO TEXT HERE -->
                <a href="{{route('site-index')}}" class="navbar-brand">{{$site_details->site_name_en}}</a>
            </div>

            <!-- MENU LINKS -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-nav-first navbar-right" >





                    <li><a href="{{route('site-index')}}" class="smoothScroll">Contact Us</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Start Buying</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Start Selling</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Services</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">About Us</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Home</a></li>
                </ul>

                <!-- <ul class="nav navbar-nav navbar-right">
                     <li><a href="#">Call Now! <i class="fa fa-phone"></i> 010 020 0340</a></li>
                     <a href="#footer" class="section-btn">Reserve a table</a>
                </ul> -->
            </div>

        </div>
    </section>

    <h1 class="text-center" style="margin-top:100px;">Choose The Device Status</h1>
    <div class="container">
        <div class="row">
            <div id="dep" style="margin-bottom: 265px;">
                @foreach($conditions as $condition)
                    <div class="col-lg-4">
                        <div class="logo">
                            <a>
                                <h4 class="get-cond" wid="{{$condition->id}}">{{$condition->name_en}}</h4> </a>

                            <a href="{{route('site.details.buy', ['status' =>$condition->name_en])}}"><button class="btn btn-primary">التالي</button></a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>

    </div>


    <div class="container">
    <div id="condition" style="clear: both;">

    </div>
    </div>


@stop


@section('js')

    <script>
        $(document).on("click", ".get-cond", function (e) {
            e.preventDefault();
            var id = $(this).attr('wid');
            var token = "{{ csrf_token() }}";

            $.ajax({
                url: "{{ route('site.get.condition') }}",
                type: "post",
                dataType: "json",
                data: {_token: token, id: id},
                success: function (data) {
                    data.user.forEach(function (val){
                        $("#condition").html('<div class="row"><div class="col-md-6"><p>' + val.cond1_en + '</p></div><div class="col-md-6"><p>' + val.cond2_en + '</p></div><div class="col-md-6"><p>' + val.cond3_en + '</p></div><div class="col-md-6"><p>' + val.cond4_en + '</p></div></div>');


                    });

                    if (data.status !== "ok") {
                        alert("ERROR");
                    }

                },
                error: function () {
                    alert("ERROR");
                }
            })
        })
    </script>


@stop