@extends('layouts.Site-Layout')

@section('title')

    Model

@stop

@section('content')

    <!-- MENU -->
    <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
        <div class="container">

            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                </button>

                <!-- lOGO TEXT HERE -->
                <a href="{{route('site-index')}}" class="navbar-brand">{{$site_details->site_name_en}}</a>
            </div>

            <!-- MENU LINKS -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-nav-first navbar-right" >





                    <li><a href="{{route('site-index')}}" class="smoothScroll">Contact Us</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Start Buying</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Start Selling</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Services</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">About Us</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Home</a></li>
                </ul>

                <!-- <ul class="nav navbar-nav navbar-right">
                     <li><a href="#">Call Now! <i class="fa fa-phone"></i> 010 020 0340</a></li>
                     <a href="#footer" class="section-btn">Reserve a table</a>
                </ul> -->
            </div>

        </div>
    </section>

    <h1 class="text-center" style="margin-top:100px;">Choose The Model</h1>
    <div class="container">
        <div class="row">
            <div id="dep">
                @foreach($models as $model)
                    <div class="col-lg-3">
                        <div class="logo">
                            <a href="{{route('site.capacity', ['model'=>$model->name_en])}}">
                                <h4>{{$model->name_en}}</h4> </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@stop


@section('js')

@stop