@extends('layouts.Site-Layout')

@section('title')

    Details

@stop

@section('content')

    <!-- MENU -->
    <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
        <div class="container">

            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                </button>

                <!-- lOGO TEXT HERE -->
                <a href="{{route('site-index')}}" class="navbar-brand">{{$site_details->site_name_en}}</a>
            </div>

            <!-- MENU LINKS -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-nav-first navbar-right" >





                    <li><a href="{{route('site-index')}}" class="smoothScroll">Contact Us</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Start Buying</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Start Selling</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Services</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">About Us</a></li>
                    <li><a href="{{route('site-index')}}" class="smoothScroll">Home</a></li>
                </ul>

                <!-- <ul class="nav navbar-nav navbar-right">
                     <li><a href="#">Call Now! <i class="fa fa-phone"></i> 010 020 0340</a></li>
                     <a href="#footer" class="section-btn">Reserve a table</a>
                </ul> -->
            </div>

        </div>
    </section>

    {{--{{dd(session()->all())}}--}}

    <div id="details" dir="rtl">
        <div class="container">
            <div class="row">
                <p class="alert alert-danger">If Price Not Appear, There is no Device With These Details</p>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                    <div class="details1">

                        @foreach($categories as $category)
                        <img src="{{asset('uploads/category/' . $category->img)}}">
@endforeach
                        @foreach ($sell_products as $sell_product)


                        @if ($session['capacity_size'] == $sell_product->memories->size && $session['status_name'] == $sell_product->conditions->name_en
                        && $session['model_name'] == $sell_product->modules->name_en
                      )


                                    <h3 class="text-center">Price <span style="font-weight:bolder;">{{$sell_product->value}} EGP </span></h3>

                                @endif
                                    @endforeach

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="details2 text-left">
                        <h3></h3>
                        <h4> Category : {{session('category_name')}}</h4>
                        <h4> Manufacture : {{session('manufacture_name')}}</h4>
                        <h4>Signal : {{session('network_name')}}</h4>
                        <h4>Model : {{session('model_name')}}</h4>
                        <h4>Capacity : {{session('capacity_size')}}</h4>
                        <h4>Status : {{session('status_name')}}</h4>

                        @foreach ($sell_products as $sell_product)


                            @if ($session['capacity_size'] == $sell_product->memories->size && $session['status_name'] == $sell_product->conditions->name_en
                            && $session['model_name'] == $sell_product->modules->name_en
                          )

                        <button class="btn btn-danger " style="text-align:center!important;margin-top:50px;width: 100px;color:white;"><a href="{{route('site.branch', ['value'=>$sell_product->value, 'img'=>$sell_product->img])}}">Confirm</a></button>

                            @endif


                        @endforeach


                </div>

            </div>

        </div>
    </div>

@stop


@section('js')

@stop