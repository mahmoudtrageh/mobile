<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="stylesheet" href="{{asset('site/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('site/css/animations.css')}}">
<link rel="stylesheet" href="{{asset('site/css/fonts.css')}}">
<link rel="stylesheet" href="{{asset('site/css/main.css')}}" class="color-switcher-link">
<link rel="stylesheet" href="{{asset('site/css/shop.css')}}" class="color-switcher-link">
<script src="{{asset('site/js/vendor/modernizr-2.6.2.min.js')}}"></script>
<!--[if lt IE 9]>
<!--<script src="js/vendor/html5shiv.min.js"></script>-->
<!--<script src="js/vendor/respond.min.js"></script>-->
<!--<script src="js/vendor/jquery-1.12.4.min.js"></script>-->
<![endif]-->