@if($errors->any())
    @if ($errors->first($field))
        {!! $errors->first($field, '<p><span class="text-warning"> * this field is required </span></p>') !!}
    @endif
@endif