
# commerce v.3

=======
****Steps To Install**** 

 1. `git clone https://gitlab.com/mahmoudtrageh/commerce_v_3.git`
 
 2. cd into the project directory 
 
 3. `composer install`
 
 4. `cp .env.example .env`
 
 5. `php artisan key:generate`
 
 6. `npm install`
 
 7. create your database and register it within .env
 
 8. `php artisan migrate --seed`
 

> Package Contains

 1. Admins
 2. Users
 3. Sliders
 4. Settings [ Site Details - Social Media - About Us - Team Work - Gallery - Services - Time ]
 5. Messages
 6. Country - City 

> User Name : iki 

> Password : 123456

