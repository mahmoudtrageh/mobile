<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = [

        'name',
        'name_en',
        'country_id',

    ];

    public function cities(){
        return $this->hasMany(City::class);
    }
}
