<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileCondition extends Model
{
    protected  $fillable=[
        'name_ar',
        'name_en',
        'cond1_ar',
        'cond1_en',
        'cond2_ar',
        'cond2_en',
        'cond3_ar',
        'cond3_en',
        'cond4_ar',
        'cond4_en',
    ];
}
