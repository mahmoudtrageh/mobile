<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingCondition extends Model
{

    protected $table = 'shipping_conditions';

    protected  $fillable=[
        'name_ar',
        'name_en',
    ];
}
