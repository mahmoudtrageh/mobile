<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{

    protected $table = 'info';
    protected  $fillable=[
        'name_ar',
        'name_en',
        'desc_ar',
        'desc_en',
        'img',
    ];
}
