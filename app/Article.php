<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'author_ar',
        'author_en',
        'date',
        'title_ar',
        'title_en',
        'content_ar',
        'content_en',
        'img',
    ];
}
