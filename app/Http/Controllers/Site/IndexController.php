<?php

namespace App\Http\Controllers\Site;

use App\BuyOrder;
use App\BuyProduct;
use App\Category;
use App\Manufacture;
use App\Memory;
use App\Message;
use App\MobileCondition;
use App\Module;
use App\Network;
use App\SellOrder;
use App\SellProduct;
use App\Store;
use App\Article;
use App\Condition;
use App\Country;
use App\CustomerLogo;
use App\Gallery;
use App\GalleryDepartment;
use App\Info;
use App\MobileColor;
use App\MobileSlider;
use App\Question;
use App\Service;
use App\ShippingCondition;
use App\SiteAbout;
use App\SiteDetail;
use App\Social;
use App\Team;
use App\Testmonials;
use App\WebColor;
use App\WebSlider;
use App\WorkDay;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index ()
    {

        $site_details = SiteDetail::first();
        $social = Social::first();
        $about = SiteAbout::first();
        $teams = Team::all();
        $gallery_departments = GalleryDepartment::all();
        $galleries = Gallery::all();
        $articles = Article::all();
        $services = Service::all();
        $countries = Country::all();
        $web_colors = WebColor::all();
        $mobile_colors = MobileColor::all();
        $web_sliders = WebSlider::all();
        $mobile_sliders = MobileSlider::all();
        $testmonials = Testmonials::all();
        $customer_logos = CustomerLogo::all();
        $conditions = Condition::all();
        $shipping_conditions = ShippingCondition::all();
        $questions = Question::all();
        $info = Info::all();
        $categories = Category::all();

        return view('site.index', compact('site_details',
            'social', 'about', 'teams', 'galleries', 'services',
            'gallery_departments', 'articles', 'countries', 'provinces',
            'cities', 'areas', 'mobile_colors',
            'web_colors', 'web_sliders',
            'mobile_sliders', 'testmonials', 'customer_logos', 'conditions',
            'questions', 'info', 'shipping_conditions', 'categories'));
    }

    // buy
    public function buy (Request $request)
    {
        $site_details = SiteDetail::first();

        $categories = Category::all();
        foreach($categories as $category)
        {
            session()->put('category_name', $category->name_en);
        }
        return view('site.buy', compact('categories', 'site_details'));
    }

    public function department (Request $request, $id)
    {
        $site_details = SiteDetail::first();

        $manufactures = Manufacture::where('category_id', $id)->get();

        session()->put('category_name', $request->category);

        return view('site.department', compact('manufactures', 'id', 'site_details'));
    }

    public function signal (Request $request)
    {
        $site_details = SiteDetail::first();

        $networks = Network::all();

        session()->put('capacity_size', $request->capacity);

        return view('site.signal', compact('networks', 'site_details'));
    }

    public function model (Request $request, $id)
    {
        $site_details = SiteDetail::first();

        $models = Module::where('manufacture_id', $id)->get();
        session()->put('manufacture_name', $request->manufacture);

        return view('site.model', compact('models', 'id', 'site_details'));
    }

    public function details (Request $request)
    {
        $site_details = SiteDetail::first();

        $sell_products = SellProduct::all();
        $session = session()->all();

        $categories = Category::where('name_en', $session['category_name'])->get();
        session()->put('status_name', $request->status);

        return view('site.details', compact('sell_products', 'session',
            'sell_one', 'categories', 'site_details'));
    }

    public function status (Request $request)
    {
        $site_details = SiteDetail::first();

        $conditions = MobileCondition::all();
        session()->put('network_name', $request->network);

        return view('site.status', compact('conditions', 'site_details'));
    }

    public function capacity (Request $request)
    {
        $site_details = SiteDetail::first();

        $memories = Memory::all();
        session()->put('model_name', $request->model);

        return view('site.capacity', compact('memories', 'site_details'));
    }

    public function branch (Request $request)
    {

        $site_details = SiteDetail::first();

        $stores = Store::all();
        session()->put('value', $request->value);
        session()->put('img', $request->img);

        return view('site.branch', compact('stores', 'site_details'));
    }

    public function SendOrder (Request $request)
    {


        $sell_orders['category_name'] = session('category_name');
        $sell_orders['manufacture_name'] = session('manufacture_name');
        $sell_orders['network'] = session('network_name');
        $sell_orders['model_name'] = session('model_name');
        $sell_orders['memory'] = session('capacity_size');
        $sell_orders['condition'] = session('status_name');
        $sell_orders ['name'] = $request->name;
        $sell_orders ['email'] = $request->email;
        $sell_orders ['phone'] = $request->phone;
        $sell_orders['value'] = session('value');
        $sell_orders['store'] = $request->store;
        $sell_orders['img'] = session('img');

        SellOrder::create($sell_orders);

        return redirect()->back()->with('success', 'Your Device Has Been Added, You will Be contacted');

        }



        // sell

    public function sell (Request $request)
    {
        $site_details = SiteDetail::first();

        $categories = Category::all();
        foreach($categories as $category)
        {
            session()->put('category_name', $category->name_en);
        }
        return view('site.sell', compact('categories', 'site_details'));
    }


    public function departmentBuy (Request $request, $id)
    {
        $site_details = SiteDetail::first();

        $manufactures = Manufacture::where('category_id', $id)->get();

        session()->put('category_name', $request->category);

        return view('site.department-buy', compact('manufactures',
            'id', 'site_details'));
    }

    public function signalBuy (Request $request)
    {
        $site_details = SiteDetail::first();

        $networks = Network::all();

        session()->put('capacity_size', $request->capacity);

        return view('site.signal-buy', compact('networks', 'site_details'));
    }

    public function modelBuy (Request $request, $id)
    {
        $site_details = SiteDetail::first();

        $models = Module::where('manufacture_id', $id)->get();
        session()->put('manufacture_name', $request->manufacture);

        return view('site.model-buy', compact('models', 'id', 'site_details'));
    }

    public function detailsBuy (Request $request)
    {
        $site_details = SiteDetail::first();

        $buy_products = BuyProduct::all();
        $session = session()->all();

        $categories = Category::where('name_en', $session['category_name'])->get();

        session()->put('status_name', $request->status);

        return view('site.details-buy', compact('buy_products',
            'session', 'sell_one', 'categories', 'site_details'));
    }

    public function statusBuy (Request $request)
    {
        $site_details = SiteDetail::first();

        $conditions = MobileCondition::all();
        session()->put('network_name', $request->network);

        return view('site.status-buy', compact('conditions', 'site_details'));
    }

    public function capacityBuy (Request $request)
    {
        $site_details = SiteDetail::first();

        $memories = Memory::all();
        session()->put('model_name', $request->model);

        return view('site.capacity-buy', compact('memories', 'site_details'));
    }

    public function branchBuy (Request $request)
    {
        $site_details = SiteDetail::first();

        $stores = Store::all();
        session()->put('value', $request->value);
        session()->put('img', $request->img);

        return view('site.branch-buy', compact('stores', 'site_details'));
    }

    public function sellMobile (Request $request)
    {


        $buy_orders['category_name'] = session('category_name');
        $buy_orders['manufacture_name'] = session('manufacture_name');
        $buy_orders['network'] = session('network_name');
        $buy_orders['model_name'] = session('model_name');
        $buy_orders['memory'] = session('capacity_size');
        $buy_orders['condition'] = session('status_name');
        $buy_orders ['name'] = $request->name;
        $buy_orders ['email'] = $request->email;
        $buy_orders ['phone'] = $request->phone;
        $buy_orders['value'] = session('value');
        $buy_orders['store'] = $request->store;
        $buy_orders['img'] = session('img');

        BuyOrder::create($buy_orders);

        return redirect()->back()->with('success', 'Your Device Has Been Added, You will Be contacted');

    }

    public function sendMessage(Request $request)
    {
        $input = $request->all();
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'title' => 'required',
            'message' => 'required',
        ],
            [
                'name.required' => 'Name Is required',
                'phone.required' => 'Phone Is required ',
                'email.required' => 'Email Is Required ',
                'title.required' => 'Title Is required ',
                'message.required' => 'Message is required ',
            ]);


        Message::create($input);
        session()->flash('success', 'Message sent successfully');
        return redirect()->back();
    }

    public function getCondition(Request $request)
    {

        $get_conditions = MobileCondition::where('id', $request->id)->get();

        return response()->json(["status" => "ok", 'user' => $get_conditions]);
    }

}
