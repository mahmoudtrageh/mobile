<?php

namespace App\Http\Controllers\Admin;

use App\Activity;
use App\Area;
use App\Article;
use App\City;
use App\Condition;
use App\Country;
use App\CustomerLogo;
use App\Gallery;
use App\GalleryDepartment;
use App\Info;
use App\MobileColor;
use App\MobileSlider;
use App\Province;
use App\Question;
use App\Seminar;
use App\Service;
use App\ShippingCondition;
use App\SiteAbout;
use App\SiteDetail;
use App\Social;
use App\Sponser;
use App\Team;
use App\Testmonials;
use App\Video;
use App\WebColor;
use App\WebSlider;
use App\WorkDay;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminPermissions:4');
    }

    public function index()
    {

        $site_details = SiteDetail::first();
        $social = Social::first();
        $about = SiteAbout::first();
        $teams = Team::all();
        $gallery_departments = GalleryDepartment::all();
        $galleries = Gallery::all();
        $articles = Article::all();
        $services = Service::all();
        $work_days = WorkDay::all();
        $countries = Country::all();
        $web_colors = WebColor::all();
        $mobile_colors = MobileColor::all();
        $web_sliders = WebSlider::all();
        $mobile_sliders = MobileSlider::all();
        $testmonials = Testmonials::all();
        $customer_logos = CustomerLogo::all();
        $conditions = Condition::all();
        $shipping_conditions = ShippingCondition::all();
        $questions = Question::all();
        $info = Info::all();
        return view('admin.pages.settings.settings', compact('site_details',
            'social', 'about', 'teams', 'galleries', 'services', 'work_days',
            'gallery_departments', 'articles', 'countries', 'provinces',
            'cities', 'areas', 'mobile_colors',
            'web_colors', 'web_sliders',
            'mobile_sliders', 'testmonials', 'customer_logos', 'conditions',
            'questions', 'info', 'shipping_conditions'));
    }


    //site details
    public function editSiteDetails(Request $request)
    {
        $checker = SiteDetail::first();
        $input = $request->all();
        if ($request->file('icon')) {
            $destination = public_path('uploads/site_details');
            $input['icon'] = update_file($request->file('icon'), $checker, 'icon', $destination);
        }
        if ($request->file('logo')) {
            $destination = public_path('uploads/site_details');
            $input['logo'] = update_file($request->file('logo'), $checker, 'logo', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    //site social
    public function editSocial(Request $request)
    {
        Social::first()->update($request->all());
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    // site about
    public function editAbout(Request $request)
    {
        $checker = SiteAbout::first();
        $input = $request->all();
        if ($request->file('img1')) {
            $destination = public_path('uploads/site_about');
            $input['img1'] = update_file($request->file('img1'), $checker, 'img1', $destination);
        }
        if ($request->file('img2')) {
            $destination = public_path('uploads/site_about');
            $input['img2'] = update_file($request->file('img2'), $checker, 'img2', $destination);
        }
        if ($request->file('img3')) {
            $destination = public_path('uploads/site_about');
            $input['img3'] = update_file($request->file('img3'), $checker, 'img3', $destination);
        }
        if ($request->file('img4')) {
            $destination = public_path('uploads/site_about');
            $input['img4'] = update_file($request->file('img4'), $checker, 'img4', $destination);
        }
        if ($request->file('img5')) {
            $destination = public_path('uploads/site_about');
            $input['img5'] = update_file($request->file('img5'), $checker, 'img5', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    // Team
    public function addTeam(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:100',
            'name' => 'required|max:100',
            'name_en' => 'required|max:100',
            'title_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),
                'title.required' =>trans('admin.job_title.required'),
                'name_en.required' =>trans('admin.name en.required'),
                'title_en.required' => trans('admin.job_title en.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/team');
        $input['img'] = add_file($request->file('img'), $destination);
        Team::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editTeam(Request $request)
    {

        $checker = Team::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/team');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteTeam(Request $request)
    {
        $checker = Team::find($request->id);
        $destination = public_path('uploads/team');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Gallery Department
    public function addGalleryDepartment(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|max:100',
            'name_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' =>trans('admin.name en.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/gallery');
        $input['img'] = add_file($request->file('img'), $destination);
        GalleryDepartment::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editGalleryDepartment(Request $request)
    {

        $checker = GalleryDepartment::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/gallery');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteGalleryDepartment(Request $request)
    {
        $checker = GalleryDepartment::find($request->id);
        $destination = public_path('uploads/gallery');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Gallery
    public function addGallrey(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'name_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name en.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/gallery');
        $input['img'] = add_file($request->file('img'), $destination);
        Gallery::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editGallrey(Request $request)
    {
        $checker = Gallery::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/gallery');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteGallrey(Request $request)
    {
        $checker = Gallery::find($request->id);
        $destination = public_path('uploads/gallery');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Gallery
    public function addArticle(Request $request)
    {
        $this->validate($request, [
            'author_ar' => 'required|max:100',
            'author_en' => 'required|max:100',
            'date' => 'required',
            'title_ar' => 'required',
            'title_en' => 'required',
            'content_ar' => 'required',
            'content_en' => 'required',
            'img' => 'required',

        ],
            [
                'author_ar.required' => trans('admin.author_ar.required'),
                'author_en.required' => trans('admin.author_en.required'),
                'date.required' => trans('admin.date.required'),
                'title_ar.required' => trans('admin.title.required'),
                'title_en.required' => trans('admin.title en.required'),
                'content_ar.required' => trans('admin.content_ar.required'),
                'content_en.required' => trans('admin.content_en.required'),
                'img.required' => trans('admin.img.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/article');
        $input['img'] = add_file($request->file('img'), $destination);
        Article::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editArticle(Request $request)
    {
        $checker = Article::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/article');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteArticle(Request $request)
    {
        $checker = Article::find($request->id);
        $destination = public_path('uploads/article');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }


    // Services
    public function addService(Request $request)
    {
        $this->validate($request, [
            'img' => 'required',
        ],
            [
                'img.required' => trans('admin.img.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/services');
        $input['img'] = add_file($request->file('img'), $destination);
        Service::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editService(Request $request)
    {
        $checker = Service::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/services');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteService(Request $request)
    {
        $checker = Service::find($request->id);
        $destination = public_path('uploads/services');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }


    public function editWebColor(Request $request)
    {
        $checker = WebColor::find($request->id);
        $input = $request->all();
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function editMobileColor(Request $request)
    {
        $checker = MobileColor::find($request->id);
        $input = $request->all();
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    // Services
    public function addWebSlider(Request $request)
    {
        $this->validate($request, [
            'img' => 'required',
        ],
            [
                'img.required' => trans('admin.img.required'),
            ]);

        $input = $request->all();
        $destination = public_path('uploads/slider');
        $input['img'] = add_file($request->file('img'), $destination);
        WebSlider::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editWebSlider(Request $request)
    {
        $checker = WebSlider::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/slider');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteWebSlider(Request $request)
    {
        $checker = WebSlider::find($request->id);
        $destination = public_path('uploads/slider');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }


    // Services
    public function addMobileSlider(Request $request)
    {
        $this->validate($request, [
            'img' => 'required',
        ],
            [
                'img.required' => trans('admin.img.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/slider');
        $input['img'] = add_file($request->file('img'), $destination);
        MobileSlider::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editMobileSlider(Request $request)
    {
        $checker = MobileSlider::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/slider');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteMobileSlider(Request $request)
    {
        $checker = MobileSlider::find($request->id);
        $destination = public_path('uploads/slider');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Services
    public function addTestmonials(Request $request)
    {
        $input = $request->all();
        $destination = public_path('uploads/testmonials');
        $input['img'] = add_file($request->file('img'), $destination);
        Testmonials::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editTestmonials(Request $request)
    {
        $checker = Testmonials::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/testmonials');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteTestmonials(Request $request)
    {
        $checker = Testmonials::find($request->id);
        $destination = public_path('uploads/testmonials');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    public function addCustomerLogo(Request $request)
    {
        $input = $request->all();
        $destination = public_path('uploads/logo');
        $input['img'] = add_file($request->file('img'), $destination);
        CustomerLogo::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editCustomerLogo(Request $request)
    {
        $checker = CustomerLogo::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/logo');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteCustomerLogo(Request $request)
    {
        $checker = CustomerLogo::find($request->id);
        $destination = public_path('uploads/logo');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    public function addCondition(Request $request)
    {
        $input = $request->all();
        Condition::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editCondition(Request $request)
    {
        $checker = Condition::find($request->id);
        $input = $request->all();
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteCondition(Request $request)
    {
        $checker = Condition::find($request->id);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    public function addShCondition(Request $request)
    {
        $input = $request->all();
        ShippingCondition::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editShCondition(Request $request)
    {
        $checker = ShippingCondition::find($request->id);
        $input = $request->all();
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteShCondition(Request $request)
    {
        $checker = ShippingCondition::find($request->id);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }


    public function addQuestion(Request $request)
    {
        $input = $request->all();
        Question::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editQuestion(Request $request)
    {
        $checker = Question::find($request->id);
        $input = $request->all();
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteQuestion(Request $request)
    {
        $checker = Question::find($request->id);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    public function addInfo(Request $request)
    {

        dd($request->file('img'));
        $input = $request->all();
        $destination = public_path('uploads/info');
        $input['img'] = add_file($request->file('img'), $destination);
        Info::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editInfo(Request $request)
    {
        $checker = Info::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/info');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteInfo(Request $request)
    {
        $checker = Info::find($request->id);
        $destination = public_path('uploads/info');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }


    // Working Days

    public function editTime(Request $request)
    {
        WorkDay::find($request->id)->update($request->all());
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function editStatus(Request $request)
    {
        $day = WorkDay::find($request->id);
        $day->is_active = $request->status;
        $day->save();
        return response()->json(["status" => "1", 'message' => 'Success']);
    }


    public function AddCountry(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name en.required'),
            ]);

        if (Country::create($request->all())) {
            return redirect()->back()->withSuccess(trans('admin.add.success'));
        }
        return redirect()->back()->withInput($request->all());

    }

    public function EditCountry(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name en.required'),
            ]);
        $checker = Country::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess(trans('admin.update.success'));
        }
        return redirect()->back()->withInput($request->all());

    }

    public function DeleteCountry(Request $request)
    {
        Country::find($request->id)->delete();
        return redirect()->back()->with('deleted', trans('admin.delete.success'));

    }

    public function Provinces($id)
    {

        $provinces = Country::find($id)->provinces;

        return view('admin.pages.settings.provinces', compact(  'provinces', 'id'));

    }

    public function AddProvince(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'name_en' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name en.required'),
            ]);

        if (Province::create($request->all())) {
            return redirect()->back()->withSuccess(trans('admin.add.success'));
        }
        return redirect()->back()->withInput($request->all());

    }

    public function EditProvince(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'name_en' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name en.required'),
            ]);
        $checker = Province::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess(trans('admin.update.success'));
        }
        return redirect()->back()->withInput($request->all());

    }

    public function DeleteProvince(Request $request)
    {
        Province::find($request->id)->delete();
        return redirect()->back()->with('deleted', trans('admin.delete.success'));

    }

    public function Cities($id)
    {

        $cities = Province::find($id)->cities;
        return view('admin.pages.settings.cities', compact('cities', 'id'));

    }

    public function AddCity(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name en.required'),
            ]);

        if (City::create($request->all())) {
            return redirect()->back()->withSuccess(trans('admin.add.success'));
        }
        return redirect()->back()->withInput($request->all());

    }

    public function EditCity(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name en.required'),
            ]);
        $checker = City::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess(trans('admin.update.success'));
        }
        return redirect()->back()->withInput($request->all());

    }

    public function DeleteCity(Request $request)
    {
        City::find($request->id)->delete();
        return redirect()->back()->with('deleted',trans('admin.delete.success'));

    }

    public function Areas($id)
    {

        $areas = City::find($id)->areas;
        return view('admin.pages.settings.areas', compact('areas', 'id'));

    }

    public function AddArea(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name en.required'),
            ]);

        if (Area::create($request->all())) {
            return redirect()->back()->withSuccess(trans('admin.add.success'));
        }
        return redirect()->back()->withInput($request->all());

    }

    public function EditArea(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name en.required'),
            ]);
        $checker = Area::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess(trans('admin.update.success'));
        }
        return redirect()->back()->withInput($request->all());

    }

    public function DeleteArea(Request $request)
    {
        Area::find($request->id)->delete();
        return redirect()->back()->with('deleted', trans('admin.delete.success'));

    }

}
