<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        $user = auth()->guard('admin')->user();
        return view('admin.pages.profile.profile', compact('user'));
    }

    public function update(Request $request)
    {
        $checker = Admin::find(auth()->guard('admin')->user()->id);
        $input = $request->all();
        $this->validate($request, [
            'name' => 'required|unique:users,name,' . $checker->id,
            'email' => 'required|unique:users,email,' . $checker->id,
        ],
            [
                'name.required' => trans('admin.name.required'),
                'name.unique' => trans('admin.name.unique'),
                'email.required' =>trans('admin.email.required'),
                'email.unique' => trans('admin.email.unique'),

            ]);
        if ($file = $request->file('img')) {
            $destination = public_path('uploads/admin/avatar');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);

        }
        if (!$file = $request->file('img')) {
            $input['img'] = $checker->img;
        }

        if ($checker->update($input)) {
            return redirect()->back()->with('success', trans('admin.update.success'));
        }
        return redirect()->back();
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required|min:6|max:15',
            'new_password_confirmation' => 'required|same:new_password',
        ],
            [
                'old_password.required' =>trans('admin.old_password.required'),
                'new_password.required' => trans('admin.new'),
                'new_password.min' => trans('admin.new_password.min'),
                'new_password.max' =>trans('admin.new_password.max'),
                'new_password_confirmation.required' =>trans('admin.new_password_confirmation.required'),
                'new_password_confirmation.same' => trans('admin.new_password_confirmation.same'),
            ]);

        $user = Admin::find(auth()->guard('admin')->user()->id);
        $input = $request->all();
        $input['new_password'] = bcrypt($request->new_password);
        if (Hash::check($request->old_password, $user->password)) {
            $user->update(['password' => $input['new_password']]);
            return redirect()->back()->with('success', trans('admin.update.success'));
        }
        return redirect()->back()->withErrors(trans('admin.invalid.old_password'));
    }
}
