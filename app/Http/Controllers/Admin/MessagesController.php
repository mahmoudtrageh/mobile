<?php

namespace App\Http\Controllers\Admin;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('adminPermissions:5');
    }

    public function index()
    {
        $messages = Message::all();
        return view('admin.pages.messages.messages', compact('messages'));
    }

    public function delete(Request $request)
    {
        Message::find($request->id)->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    public function deleteAll()
    {
        Message::truncate();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }
}
