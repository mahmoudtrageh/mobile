<?php

namespace App\Http\Controllers\Admin\profileManagement;

use App\CustomerGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerGroupsController extends Controller
{
    public function index()
    {
        $groups = CustomerGroup::all();
        return view('admin.pages.profile-management.customers.customer-group.index', compact('groups'));
    }

    public function getAdd(Request $request)
    {
        if ($request->id) {
            $group = CustomerGroup::find($request->id);
            return view('admin.pages.profile-management.customers.customer-group.add-edit', compact('group'));
        } else
            return view('admin.pages.profile-management.customers.customer-group.add-edit');
    }

    public function add_edit(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
            'description_ar' => 'required',
            'description_en' => 'required',
        ]);
        if ($request->id) {
            CustomerGroup::find($request->id)->update($request->except('id'));
        } else {
            CustomerGroup::create($request->all());
        }
        return redirect()->back()->with('success', 'success');
    }

    public function delete(Request $request)
    {
        CustomerGroup::find($request->id)->delete();
        return redirect()->back()->with('success', 'success');
    }


}
