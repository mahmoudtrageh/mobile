<?php

namespace App\Http\Controllers\Admin\profileManagement;

use App\Customer;
use App\CustomerGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomersController extends Controller
{
    public function index()
    {
        return view('admin.pages.profile-management.customers.index');
    }

    public function getAdd()
    {
        $groups = CustomerGroup::all();
        return view('admin.pages.profile-management.customers.add-edit', compact('groups'));
    }

    public function add_edit(Request $request)
    {
        $this->validate($request, [
            'group_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
            'status' => 'required',
            'safe' => 'required',
            'newsletter' => 'required',

        ]);

//        if ($request->id) {
//            CustomerGroup::find($request->id)->update($request->except('id'));
//        } else {

            Customer::create($request->all());
//        }
        return redirect()->back()->with('success', 'success');
    }

}
