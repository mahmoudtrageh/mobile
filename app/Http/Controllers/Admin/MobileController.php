<?php

namespace App\Http\Controllers\Admin;

use App\BuyOrder;
use App\BuyProduct;
use App\Category;
use App\Manufacture;
use App\Memory;
use App\MobileCondition;
use App\Module;
use App\Network;
use App\SellOrder;
use App\SellProduct;
use App\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class MobileController extends Controller
{
    public function index()
    {

        $categories = Category::all();
        $manufactures = Manufacture::all();
        $modules = Module::all();
        $memories = Memory::all();
        $networks = Network::all();
        $mobile_conditions = MobileCondition::all();
        $stores = Store::all();
        $sell_products = SellProduct::all();
        $buy_products = BuyProduct::all();
        $orders_sell = SellOrder::all();
        $orders_buy = BuyOrder::all();
        $session = session()->all();

        return view('admin.pages.mobile.index', compact( 'categories', 'manufactures', 'modules', 'memories',
            'networks', 'mobile_conditions', 'stores',
            'sell_products', 'buy_products', 'orders_sell', 'product_img', 'session', 'orders_buy'));
    }

    // Category
    public function addCategory(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|max:100',
            'name_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' =>trans('admin.name en.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/category');
        $input['img'] = add_file($request->file('img'), $destination);
        Category::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editCategory(Request $request)
    {

        $checker = Category::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/category');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteCategory(Request $request)
    {
        $checker = Category::find($request->id);
        $destination = public_path('uploads/category');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    public function deleteSell(Request $request)
    {
        $checker = SellOrder::find($request->id);
        $destination = public_path('uploads/sell-product');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    public function editSell(Request $request)
    {
        $day = SellOrder::find($request->id);
        $day->is_contacted = $request->status;
        $day->save();
        return response()->json(["status" => "1", 'message' => 'Success']);
    }

    public function deleteBuy(Request $request)
    {
        $checker = BuyOrder::find($request->id);
        $destination = public_path('uploads/sell-product');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    public function editBuy(Request $request)
    {
        $day = BuyOrder::find($request->id);
        $day->is_contacted = $request->status;
        $day->save();
        return response()->json(["status" => "1", 'message' => 'Success']);
    }


    // Manufacture
    public function addManufacture(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|max:100',
            'name_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' =>trans('admin.name en.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/manufacture');
        $input['img'] = add_file($request->file('img'), $destination);
        Manufacture::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editManufacture(Request $request)
    {

        $checker = Manufacture::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/manufacture');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteManufacture(Request $request)
    {
        $checker = Manufacture::find($request->id);
        $destination = public_path('uploads/manufacture');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Module
    public function addModule(Request $request)
    {

        $this->validate($request, [
            'name_ar' => 'required|max:100',
            'name_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' =>trans('admin.name en.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/module');
        $input['img'] = add_file($request->file('img'), $destination);
        Module::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editModule(Request $request)
    {
        $checker = Module::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/module');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteModule(Request $request)
    {
        $checker = Module::find($request->id);
        $destination = public_path('uploads/module');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Module
    public function addMemory(Request $request)
    {

        $this->validate($request, [
            'size' => 'required',
        ],
            [
                'size.required' => trans('admin.name.required'),
            ]);
        $input = $request->all();
        Memory::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editMemory(Request $request)
    {
        $checker = Memory::find($request->id);
        $input = $request->all();
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteMemory(Request $request)
    {
        $checker = Memory::find($request->id);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Module
    public function addNetwork(Request $request)
    {

        $this->validate($request, [
            'name_ar' => 'required|max:100',
            'name_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' =>trans('admin.name en.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/network');
        $input['img'] = add_file($request->file('img'), $destination);
        Network::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editNetwork(Request $request)
    {
        $checker = Network::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/network');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteNetwork(Request $request)
    {
        $checker = Network::find($request->id);
        $destination = public_path('uploads/network');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }


    public function addMobileCondition(Request $request)
    {

        $this->validate($request, [
            'name_ar' => 'required|max:100',
            'name_en' => 'required|max:100',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' =>trans('admin.name en.required'),
            ]);
        $input = $request->all();
        MobileCondition::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editMobileCondition(Request $request)
    {
        $checker = MobileCondition::find($request->id);
        $input = $request->all();
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteMobileCondition(Request $request)
    {
        $checker = MobileCondition::find($request->id);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Module
    public function addStore(Request $request)
    {

        $this->validate($request, [
            'name_ar' => 'required|max:100',
            'name_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' =>trans('admin.name en.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/store');
        $input['img'] = add_file($request->file('img'), $destination);
        Store::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editStore(Request $request)
    {
        $checker = Store::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/store');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteStore(Request $request)
    {
        $checker = Store::find($request->id);
        $destination = public_path('uploads/store');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Module
    public function addSellProduct(Request $request)
    {

        $this->validate($request, [
            'img' => 'required',
            'value' => 'required',
        ],
            [
                'img.required' => trans('admin.phone.required'),
                'value.required' => trans('admin.phone.required'),

            ]);
        $input = $request->all();
        $destination = public_path('uploads/sell-product');
        $input['img'] = add_file($request->file('img'), $destination);
        SellProduct::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editSellProduct(Request $request)
    {
        $checker = SellProduct::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/sell-product');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteSellProduct(Request $request)
    {
        $checker = SellProduct::find($request->id);
        $destination = public_path('uploads/sell-product');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Module
    public function addBuyProduct(Request $request)
    {

        $this->validate($request, [
            'img' => 'required',
            'value' => 'required',
        ],
            [
                'img.required' => trans('admin.phone.required'),
                'value.required' => trans('admin.phone.required'),

            ]);
        $input = $request->all();
        $destination = public_path('uploads/buy-product');
        $input['img'] = add_file($request->file('img'), $destination);
        BuyProduct::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editBuyProduct(Request $request)
    {
        $checker = BuyProduct::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/buy-product');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteBuyProduct(Request $request)
    {
        $checker = BuyProduct::find($request->id);
        $destination = public_path('uploads/buy-product');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }


}
