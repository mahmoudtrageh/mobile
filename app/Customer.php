<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
     protected  $fillable=[

             'group_id',
             'name',
             'email',
             'phone',
             'password',
             'status',
             'safe',
             'newsletter',
             'gender',
             'company',
             'website',
             'tracking_code',
             'commission',
             'tax_id',
             'cn_id',
         ];
}
