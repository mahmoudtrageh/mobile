<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryDepartment extends Model
{

    protected $table = 'gallery_departments';

    protected  $fillable=[

        'name_ar',
        'name_en',
        'img',
    ];
}
