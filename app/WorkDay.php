<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkDay extends Model
{
     protected  $fillable=[

             'day',
             'from',
             'to',
             'from_timing',
             'to_timing',
             'is_active',

         ];
}
