<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerLogo extends Model
{

    protected $table = 'customer_logo';

    protected  $fillable=[

        'img',
        'name_en',
        'name_ar',
    ];
}
