<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'galleries';

    protected  $fillable=[

        'name',
        'text',
        'img',
        'name_en',
        'text_en',
       'department_id',
    ];

    public function departments(){
        return $this->belongsTo('App\GalleryDepartment', 'department_id');
    }
}
