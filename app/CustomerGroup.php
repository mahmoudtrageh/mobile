<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerGroup extends Model
{
     protected  $fillable=[

             'name_ar',
             'name_en',
             'description_ar',
             'description_en',
             'pre_approved',
             'sort_order',

         ];
}
