<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileSlider extends Model
{
    protected  $fillable=[
        'name_ar',
        'name_en',
        'img',
        'is_active',
    ];
}
