<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [

        'name_ar',
        'name_en',
        'img',
        'manufacture_id',
    ];

    public function manufactures(){
        return $this->belongsTo('App\Manufacture', 'manufacture_id');
    }
}
