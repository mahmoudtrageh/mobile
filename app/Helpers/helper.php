<?php

if (!function_exists('add_file')) {
    function add_file($request_file_name, $destination)
    {
        if ($file = $request_file_name) {
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move($destination, $name);
        }
        return $name;
    }
}

if (!function_exists('update_file')) {
    function update_file($request_file_name, $checker, $database_name, $destination)
    {
        if ($file = $request_file_name) {
            @unlink($destination . '/' . $checker->$database_name);
            $name = rand(0000, 9999) . time() . '.' . $file->getClientOriginalExtension();
            $file->move($destination, $name);
        }
        return $name;
    }
}

if (!function_exists('delete_file')) {
    function delete_file($checker, $database_name, $destination)
    {
        @unlink($destination . '/' . $checker->$database_name);
    }
}

if (!function_exists('adminPermissions')) {
    function adminPermissions($role_id)
    {
        $user = auth()->guard('admin')->user();
        if (in_array($role_id, $user->roles->pluck('id')->toArray())
            || $user->isSuper) {
            return true;
        }
        return false;
    }
}

if (!function_exists('sidebarActive')) {
    function sidebarActive($route)
    {
        $route =  is_array($route) ? $route : explode('|',$route);
        if (in_array(Route::currentRouteName() , $route)) {
            return true;
        }
        return false;
    }
}

if (!function_exists('site_details')) {
    function site_details()
    {
        $details = \App\SiteDetail::first();
        return $details;
    }
};

if (!function_exists('socials')) {
    function socials()
    {
        $socials = \App\Social::first();
        return $socials;
    }
};


if (!function_exists('work_days')) {
    function work_days()
    {
        $work_days = \App\WorkDay::where('is_active', 1)->get();

        return $work_days;
    }
};