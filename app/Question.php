<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected  $fillable=[
        'question_ar',
        'question_en',
        'answer_ar',
        'answer_en',
    ];
}
