<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellOrder extends Model
{
    protected $fillable = [

        'category_name',
        'manufacture_name',
        'model_name',
        'memory',
        'network',
        'condition',
        'store',
        'value',
        'img',
        'is_contacted',
        'name',
        'phone',
        'email',
    ];

}
