<?php

namespace App;

use Hamcrest\Thingy;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [

        'name_ar',
        'name_en',

    ];

    public function provinces()
    {
        return $this->hasMany(Province::class);
    }

}
