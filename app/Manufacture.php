<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacture extends Model
{
    protected $fillable = [

        'name_ar',
        'name_en',
        'img',
        'category_id',
    ];

    public function categories(){
        return $this->belongsTo('App\Category', 'category_id');
    }
}
