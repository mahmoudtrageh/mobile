<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellProduct extends Model
{
    protected $fillable = [

        'category_id',
        'manufacture_id',
        'module_id',
        'memory_id',
        'network_id',
        'condition_id',
        'value',
        'img',
    ];

    public function manufactures(){
        return $this->belongsTo('App\Manufacture', 'manufacture_id');
    }

    public function categories(){
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function modules(){
        return $this->belongsTo('App\Module', 'module_id');
    }

    public function memories(){
        return $this->belongsTo('App\Memory', 'memory_id');
    }

    public function networks(){
        return $this->belongsTo('App\Network', 'network_id');
    }

    public function conditions(){
        return $this->belongsTo('App\MobileCondition', 'condition_id');
    }
}
