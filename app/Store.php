<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = [

        'name_ar',
        'name_en',
        'img',
        'address_ar',
        'address_en',
    ];
}
