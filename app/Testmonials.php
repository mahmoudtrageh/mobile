<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testmonials extends Model
{
    protected  $fillable=[
        'name_ar',
        'name_en',
        'img',
        'say_ar',
        'say_en',
    ];
}
