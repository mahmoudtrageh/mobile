<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyProduct extends Model
{
    protected $fillable = [

        'category_id',
        'manufacture_id',
        'module_id',
        'memory_id',
        'network_id',
        'condition_id',
        'value',
        'img',
    ];

    public function manufacturesb(){
        return $this->belongsTo('App\Manufacture', 'manufacture_id');
    }

    public function categoriesb(){
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function modulesb(){
        return $this->belongsTo('App\Module', 'module_id');
    }

    public function memoriesb(){
        return $this->belongsTo('App\Memory', 'memory_id');
    }

    public function networksb(){
        return $this->belongsTo('App\Network', 'network_id');
    }

    public function conditionsb(){
        return $this->belongsTo('App\MobileCondition', 'condition_id');
    }
}
