<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteDetail extends Model
{
     protected  $fillable=[

             'site_name',
             'address',
         'address_en',
         'site_name_en',
         'phone',
             'email',
             'logo',
             'icon',
         'map',
     ];
}
